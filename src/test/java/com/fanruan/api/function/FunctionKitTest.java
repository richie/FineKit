package com.fanruan.api.function;

import com.fanruan.api.Prepare;
import com.fanruan.api.function.shell.FineFunc;
import com.fr.script.Calculator;
import com.fr.stable.UtilEvalError;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 */
public class FunctionKitTest extends Prepare {

    @Test
    public void testFuncAdd() {
        FunctionKit.removeFunc("com.fanruan.api.function.TestFunc");
        FunctionKit.addFunc(new FineFunc("TestFunc", "com.fanruan.api.function.TestFunc"));
        try {
            assertEquals("OK", Calculator.createCalculator().evalValue("TESTFUNC()"));
        } catch (UtilEvalError utilEvalError) {

        }

    }

    @Test
    public void testFuncRemove() {
        FunctionKit.removeFunc("com.fanruan.api.function.TestFunc");
        FunctionKit.addFunc(new FineFunc("TestFunc", "com.fanruan.api.function.TestFunc"));
        FunctionKit.removeFunc("com.fanruan.api.function.TestFunc");
        String result = "ERROR";
        try {
            result = Calculator.createCalculator().evalValue("TESTFUNC()").toString();
        } catch (Throwable e) {

        }
        assertNotEquals("OK", result);
    }

}