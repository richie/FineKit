package com.fanruan.api;

import com.fr.config.dao.DaoContext;
import com.fr.config.dao.impl.LocalClassHelperDao;
import com.fr.config.dao.impl.LocalEntityDao;
import com.fr.config.dao.impl.LocalXmlEntityDao;
import com.fr.runtime.FineRuntime;
import com.fr.store.StateHubManager;
import com.fr.store.impl.MemoryLock;
import com.fr.store.impl.MemoryStore;
import com.fr.transaction.Configurations;
import com.fr.transaction.LocalConfigurationHelper;
import org.junit.After;
import org.junit.Before;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class Prepare {

    @Before
    public void start() {
        FineRuntime.start();
        DaoContext.setEntityDao(new LocalEntityDao());
        DaoContext.setClassHelperDao(new LocalClassHelperDao());
        DaoContext.setXmlEntityDao(new LocalXmlEntityDao());
        Configurations.setHelper(new LocalConfigurationHelper());
        StateHubManager.setStorage(new MemoryStore());
        StateHubManager.setLock(new MemoryLock());
    }

    @After
    public void stop() {
        DaoContext.setEntityDao(null);
        DaoContext.setClassHelperDao(null);
        DaoContext.setXmlEntityDao(null);
        Configurations.setHelper(null);
        StateHubManager.setStorage(null);
        StateHubManager.setLock(null);
    }

}
