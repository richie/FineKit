package com.fanruan.api.session;

import com.fr.stable.script.CalculatorProvider;
import com.fr.stable.web.WebContextProvider;
import com.fr.web.session.SessionIDInfo;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 */
class MockSession extends SessionIDInfo {

    MockSession(String sessionID) {
        this.sessionID = sessionID;
    }

    @Override
    public String getWebTitle() {
        return null;
    }

    @Override
    public String getRelativePath() {
        return null;
    }

    @Override
    public WebContextProvider getWebContext() {
        return null;
    }

    @Override
    public Object resolveVariable(Object var, CalculatorProvider cal) {
        return null;
    }

    @Override
    public Object getParameterValue(String parameterName) {
        return null;
    }

    @Override
    public void release() {

    }
}
