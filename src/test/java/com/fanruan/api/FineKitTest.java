package com.fanruan.api;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class FineKitTest {

    @Test
    public void version() {
        Assert.assertEquals("10.0", FineKit.version());
        Assert.assertEquals("20190815", FineKit.build());
    }
}