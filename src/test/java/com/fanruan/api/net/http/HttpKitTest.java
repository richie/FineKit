package com.fanruan.api.net.http;

import com.fanruan.api.Prepare;
import com.fanruan.api.macro.EncodeConstants;
import com.fanruan.api.net.http.rs.HttpRequest;
import com.fanruan.api.net.http.rs.HttpResponseType;
import com.fanruan.api.net.http.rs.StreamResponseHandle;
import com.fanruan.api.util.IOKit;
import com.fr.json.JSONObject;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-29
 */
public class HttpKitTest extends Prepare {

    private static MockWebServer server = new MockWebServer();

    @AfterClass
    public static void tearDown() throws Exception {
        server.shutdown();
    }

    @Test
    public void testGet() {
        String text = null;
        try {
            text = HttpKit.get("http://www.baidu.com");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(text);
    }

    @Test
    public void testPost() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("key", "bbs");
        try {
            String resText = HttpKit.post("https://cloud.fanruan.com/site", map);
            assertEquals("http://bbs.fanruan.com/", new JSONObject(resText).get("value"));
        } catch (SocketTimeoutException ignore) {
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testStream() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("key", "bbs");
        try {
            InputStream in = HttpKit.post("https://cloud.fanruan.com/site", map, HttpResponseType.STREAM);
            String text = IOKit.inputStream2String(in, StandardCharsets.UTF_8);
            assertEquals("{\"value\":\"http://bbs.fanruan.com/\"}", text);
            String text2 = IOKit.inputStream2String(in, EncodeConstants.ENCODING_UTF_8);
            assertEquals("{\"value\":\"http://bbs.fanruan.com/\"}", text2);
        } catch (SocketTimeoutException ignore) {
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testStreamMock() {
        server.enqueue(new MockResponse().setBody("{\"value\":\"http://bbs.fanruan.com/\"}"));
        String url = server.url("/site").toString();
        Map<String, String> map = new HashMap<String, String>();
        map.put("key", "bbs");
        try {
            InputStream in = HttpKit.executeAndParse(HttpRequest
                            .custom()
                            .url(url)
                            .post(map)
                            .build(),
                    new StreamResponseHandle());
            String text = IOKit.inputStream2String(in, StandardCharsets.UTF_8);
            RecordedRequest takeRequest = server.takeRequest();
            assertEquals("{\"value\":\"http://bbs.fanruan.com/\"}", text);
            assertEquals("POST", takeRequest.getMethod());
            assertEquals("key=bbs", takeRequest.getBody().readUtf8());
        } catch (SocketTimeoutException ignore) {
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMethod() throws Exception {
        server.enqueue(new MockResponse().setBody("get"));
        server.enqueue(new MockResponse().setBody("post"));
        server.enqueue(new MockResponse().setBody("put"));
        server.enqueue(new MockResponse().setBody("delete"));
        String url = server.url("/v1/chat/").toString();
        HttpKit.get(url);
        assertEquals(server.takeRequest().getMethod(), "GET");
        HttpKit.post(url, Collections.<String, String>emptyMap());
        assertEquals(server.takeRequest().getMethod(), "POST");
        HttpKit.executeAndParse(HttpRequest.custom().url(url).put(Collections.<String, String>emptyMap()).build());
        assertEquals(server.takeRequest().getMethod(), "PUT");
        HttpKit.executeAndParse(HttpRequest.custom().url(url).delete().build());
        assertEquals(server.takeRequest().getMethod(), "DELETE");
    }

    @Test
    public void testHeader() throws Exception {
        server.enqueue(new MockResponse().setBody("hello, world!"));
        HttpUrl baseUrl = server.url("/v1/chat/");

        HashMap<String, String> headers = new HashMap<String, String>(1);
        headers.put("Authorization", "abc");
        String s = HttpKit.executeAndParse(HttpRequest.custom().url(baseUrl.toString()).post(Collections.<String, String>emptyMap()).headers(headers).build());
        assertEquals("hello, world!", s);
        // 测试请求头
        RecordedRequest request = server.takeRequest();
        assertEquals(request.getHeader("Authorization"), "abc");
        assertEquals("POST /v1/chat/ HTTP/1.1", request.getRequestLine());
    }

    @Test
    public void testParams() throws Exception {
        server.enqueue(new MockResponse().setBody("hello, world!"));
        HttpUrl baseUrl = server.url("/v1/chat/");

        HashMap<String, String> params = new HashMap<String, String>(1);
        params.put("key", "value");
        String s = HttpKit.executeAndParse(HttpRequest.custom().url(baseUrl.toString()).post(params).build());
        assertEquals("hello, world!", s);
        // 测试参数
        RecordedRequest request = server.takeRequest();
        assertEquals("key=value", request.getBody().readUtf8());
        assertEquals("POST /v1/chat/ HTTP/1.1", request.getRequestLine());
    }
}