package com.fanruan.api.net;

import com.fanruan.api.Prepare;
import com.fr.decision.webservice.url.alias.URLAlias;
import com.fr.decision.webservice.url.alias.impl.PluginURLAlias;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class URLAliasKitTest extends Prepare {

    @Test
    public void raw() {
        URLAlias alias = URLAliasKit.createRawAlias("/foo", "/aaa/bbb/foo");
        Assert.assertEquals(alias.getShortPath(), "/foo");
        Assert.assertEquals(alias.getTargetURL(), "/aaa/bbb/foo");
        Assert.assertFalse(alias.isWideRange());
    }

    @Test
    public void raw2() {
        URLAlias alias = URLAliasKit.createRawAlias("/foo", "/aaa/bbb/foo", true);
        Assert.assertEquals(alias.getShortPath(), "/foo");
        Assert.assertEquals(alias.getTargetURL(), "/aaa/bbb/foo");
        Assert.assertTrue(alias.isWideRange());
    }

    @Test
    public void decision() {
        URLAlias alias = URLAliasKit.createDecisionAlias("/foo", "/aaa/bbb/foo");
        Assert.assertEquals(alias.getShortPath(), "/foo");
        Assert.assertEquals(alias.getTargetURL(), "/decision/aaa/bbb/foo");
        Assert.assertFalse(alias.isWideRange());
    }

    @Test
    public void decision2() {
        URLAlias alias = URLAliasKit.createDecisionAlias("/foo", "/aaa/bbb/foo", true);
        Assert.assertEquals(alias.getShortPath(), "/foo");
        Assert.assertEquals(alias.getTargetURL(), "/decision/aaa/bbb/foo");
        Assert.assertTrue(alias.isWideRange());
    }

    @Test
    public void plugin() {
        URLAlias alias = URLAliasKit.createPluginAlias("/foo", "/aaa/bbb/foo");
        ((PluginURLAlias)alias).setPluginId("abcd");
        Assert.assertEquals(alias.getShortPath(), "/foo");
        Assert.assertEquals(alias.getTargetURL(), "/decision/plugin/private/abcd/aaa/bbb/foo");
        Assert.assertFalse(alias.isWideRange());
    }

    @Test
    public void plugin2() {
        URLAlias alias = URLAliasKit.createPluginAlias("/foo", "/aaa/bbb/foo", true);
        ((PluginURLAlias)alias).setPluginId("abcd");
        Assert.assertEquals(alias.getShortPath(), "/foo");
        Assert.assertEquals(alias.getTargetURL(), "/decision/plugin/public/abcd/aaa/bbb/foo");
        Assert.assertFalse(alias.isWideRange());
    }

    @Test
    public void plugin3() {
        URLAlias alias = URLAliasKit.createPluginAlias("/foo", "/aaa/bbb/foo", true, true);
        ((PluginURLAlias)alias).setPluginId("abcd");
        Assert.assertEquals(alias.getShortPath(), "/foo");
        Assert.assertEquals(alias.getTargetURL(), "/decision/plugin/public/abcd/aaa/bbb/foo");
        Assert.assertTrue(alias.isWideRange());
    }

}