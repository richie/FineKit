package com.fanruan.api.report.analy;

import com.fanruan.api.ModulePrepare;
import com.fanruan.api.report.analy.data.TreeNode;
import com.fr.log.FineLoggerFactory;
import com.fr.main.impl.WorkBook;
import com.fr.main.workbook.ResultWorkBook;
import com.fr.stable.ActorConstants;
import com.fr.stable.ActorFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class AnalyKitTest extends ModulePrepare {
    @Test
    public void testTree() {
        WorkBook tpl = new WorkBook();
        Map<Integer, TreeNode> nodeMap = new HashMap<Integer, TreeNode>(0);
        try {
            tpl.readStream(AnalyKitTest.class.getResourceAsStream("/com/fanruan/api/report/tree.cpt"));
            ResultWorkBook resultWorkBook = tpl.execute(java.util.Collections.EMPTY_MAP, ActorFactory.getActor(ActorConstants.TYPE_ANALYSIS));
            nodeMap = AnalyKit.generateResultBookTree(resultWorkBook, 0);

        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        Assert.assertEquals(nodeMap.toString(), "{0=0#-1#[-1,1,3,8,14,19,25,31], 1=1#0#[-1,2], 3=3#0#[-1,4,5,6,7], 19=19#0#[-1,20,21,22,23,24], 8=8#0#[-1,9,10,11,12,13], 25=25#0#[-1,26,27,28,29,30], 14=14#0#[-1,15,16,17,18], 31=31#0#[-1,32,33,34,35,36,37,38,39]}");
    }
}