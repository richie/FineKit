package com.fanruan.api.data;

import com.fanruan.api.conf.HolderKit;
import com.fr.base.AbstractTableData;
import com.fr.config.holder.Conf;
import com.fr.general.data.DataModel;
import com.fr.script.Calculator;
import com.fr.stable.ParameterProvider;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class TestTD extends AbstractTableData {

    private Conf<Integer> count = HolderKit.simple(10);

    public TestTD() {

    }

    @Override
    public ParameterProvider[] getParameters(Calculator calculator) {
        return new ParameterProvider[0];
    }

    @Override
    public DataModel createDataModel(Calculator calculator) {
        return DataModel.EMPTY_DATAMODEL;
    }

    @Override
    public DataModel createDataModel(Calculator calculator, String s) {
        return DataModel.EMPTY_DATAMODEL;
    }

    @Override
    public DataModel createDataModel(Calculator calculator, int i) {
        return DataModel.EMPTY_DATAMODEL;
    }

    @Override
    public void readXML(XMLableReader xmLableReader) {

    }

    @Override
    public void writeXML(XMLPrintWriter xmlPrintWriter) {

    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof TestTD;
    }
}
