package com.fanruan.api.data;

import com.fanruan.api.Prepare;
import com.fr.base.TableData;
import com.fr.data.TableDataSource;
import com.fr.file.TableDataConfig;
import com.fr.main.impl.WorkBook;
import com.fr.script.Calculator;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-15
 */
public class TableDataKitTest extends Prepare {

    @Test
    public void getTableData() {
        TestTD td = new TestTD();
        TableDataConfig.getInstance().addTableData("first", td);
        Calculator calculator = Calculator.createCalculator();
        calculator.setAttribute(TableDataSource.KEY, new WorkBook());
        TableData r = TableDataKit.getTableData(calculator, "first");
        Assert.assertEquals(td, r);
    }

    @Test
    public void testGetTableData() {
        TestTD td = new TestTD();
        WorkBook workBook = new WorkBook();
        workBook.putTableData("test", td);
        TableData r = TableDataKit.getTableData(workBook, "test");
        Assert.assertEquals(td, r);
    }

}