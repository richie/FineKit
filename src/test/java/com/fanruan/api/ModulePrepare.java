package com.fanruan.api;

import com.fr.base.operator.common.CommonOperator;
import com.fr.cluster.engine.activator.standalone.StandaloneModeActivator;
import com.fr.config.activator.BaseDBActivator;
import com.fr.config.activator.ConfigurationActivator;
import com.fr.env.operator.CommonOperatorImpl;
import com.fr.general.I18nResource;
import com.fr.module.Module;
import com.fr.module.tool.ActivatorToolBox;
import com.fr.report.ReportActivator;
import com.fr.report.RestrictionActivator;
import com.fr.report.module.ReportBaseActivator;
import com.fr.scheduler.SchedulerActivator;
import com.fr.store.StateServerActivator;
import com.fr.workspace.simple.SimpleWork;
import org.junit.After;
import org.junit.Before;

/**
 * @author zack
 * @date 2019/8/26
 * @version 10.0
 */
public class ModulePrepare {
    Module module = null;

    @Before
    public void start() {
        module = ActivatorToolBox.simpleLink(
                new BaseDBActivator(),
                new ConfigurationActivator(),
                new StandaloneModeActivator(),
                new StateServerActivator(),
                new SchedulerActivator(),
                new ReportBaseActivator(),
                new RestrictionActivator(),
                new ReportActivator()
        );
        SimpleWork.supply(CommonOperator.class, new CommonOperatorImpl());
        I18nResource.getInstance();
        module.start();
    }

    @After
    public void stop() {
        if (module != null) {
            module.stop();
        }
    }
}