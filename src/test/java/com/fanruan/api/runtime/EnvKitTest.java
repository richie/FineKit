package com.fanruan.api.runtime;

import com.fanruan.api.Prepare;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class EnvKitTest extends Prepare {

    @Test
    public void getWorkResource() {
        String path = "usr/shared/webroot/WEB-INF";
        EnvKit.setCurrent(path);
        boolean r = EnvKit.getWorkResource().exist("a/b.txt");
        Assert.assertFalse(r);
    }

    @Test
    public void getAndSetCurrent() {
        String path = "/usr/shared/webroot";
        try {
            EnvKit.setCurrent(path);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        path = "usr/shared/webroot/WEB-INF";
        EnvKit.setCurrent(path);
        Assert.assertEquals(EnvKit.getCurrent().getPath(), path);
    }

    @Test
    public void exit() {
        String path = "usr/shared/webroot/WEB-INF";
        EnvKit.setCurrent(path);
        EnvKit.checkout();
        Assert.assertNull(EnvKit.getCurrent());
        Assert.assertNull(EnvKit.getWorkResource());
    }
}