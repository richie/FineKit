package com.fanruan.api.cal;

import com.fanruan.api.Prepare;
import com.fr.base.Parameter;
import com.fr.script.Calculator;
import com.fr.stable.ParameterProvider;
import com.fr.stable.UtilEvalError;
import com.fr.stable.script.NameSpace;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 */
public class ParameterKitTest extends Prepare {

    @Test
    public void analyze4ParametersFromFormula() {
        ParameterProvider[] providers = ParameterKit.analyze4ParametersFromFormula("=sum($p1, $p2) + max($p4 - 1, pow(2, $p5))");
        TreeMap<String, Object> map = new TreeMap<>();
        for (ParameterProvider provider : providers) {
            map.put(provider.getName(), provider.getValue());
        }
        String[] names = map.keySet().toArray(new String[0]);
        Assert.assertArrayEquals(new String[]{"p1", "p2", "p4", "p5"}, names);
    }

    @Test
    public void analyze4Parameters() {
        String sql = "SELECT * FROM Equipment where 'Tel' = ${if(true, 1, 2)}";
        ParameterProvider[] parameters = ParameterKit.analyze4Parameters(sql, false);
        Assert.assertEquals(0, parameters.length);
        parameters = ParameterKit.analyze4Parameters(sql, true);
        Assert.assertEquals(0, parameters.length);
        sql = "SELECT * FROM s订单  where 1=1 ${if(len(aa)==0,\"\",\" and 货主城市 in (\"+\"'\"+treelayer(aa,true,\"\\',\\'\")+\"'\"+\")\")}";
        parameters = ParameterKit.analyze4Parameters(sql, false);
        Assert.assertEquals(1, parameters.length);
        Assert.assertEquals(parameters[0].getName(), "aa");
        sql = "SELECT * FROM Equipment where 'Tel' = ${if(false, 1, 2)}";
        parameters = ParameterKit.analyze4Parameters(sql, false);
        Assert.assertEquals(0, parameters.length);
        parameters = ParameterKit.analyze4Parameters(sql, true);
        Assert.assertEquals(0, parameters.length);
        sql = "SELECT * FROM s订单  where 1=1 ${if(len(aa)==0,\"\",\" and 货主城市 in (\"+\"'\"+treelayer(aa,false,\"\\',\\'\")+\"'\"+\")\")}";
        parameters = ParameterKit.analyze4Parameters(sql, false);
        Assert.assertEquals(1, parameters.length);
        Assert.assertEquals(parameters[0].getName(), "aa");
        sql = "SELECT * FROM s订单  where 1=1 ${if(len(aa)==0,\"\",\" and 货主城市 in (\"+\"'\"+treelayer(aa,\"true\",\"\\',\\'\")+\"'\"+\")\")}";
        parameters = ParameterKit.analyze4Parameters(sql, false);
        Assert.assertEquals(1, parameters.length);
        Assert.assertEquals(parameters[0].getName(), "aa");

    }

    @Test
    public void createParameterMapNameSpace() {
        Map<String, Object> map = new HashMap<>();
        map.put("p1", 100);
        map.put("p2", 200);
        NameSpace ns = ParameterKit.createParameterMapNameSpace(map);
        Calculator calculator = Calculator.createCalculator();
        calculator.pushNameSpace(ns);
        try {
            Object r = calculator.eval("=$p1 + $p2");
            Assert.assertEquals(300, r);
        } catch (UtilEvalError utilEvalError) {
            Assert.fail();
        } finally {
            calculator.removeNameSpace(ns);
        }
    }

    @Test
    public void createParameterMapNameSpace2() {
        ParameterProvider[] parameters = new ParameterProvider[]{
                ParameterKit.newParameter("p1", 100),
                ParameterKit.newParameter("p2", 200)
        };
        NameSpace ns = ParameterKit.createParameterMapNameSpace(parameters);
        Calculator calculator = Calculator.createCalculator();
        calculator.pushNameSpace(ns);
        try {
            Object r = calculator.eval("=$p1 + $p2");
            Assert.assertEquals(300, r);
        } catch (UtilEvalError utilEvalError) {
            Assert.fail();
        } finally {
            calculator.removeNameSpace(ns);
        }
    }

    @Test
    public void newParameter() {
        ParameterProvider provider = ParameterKit.newParameter();
        Assert.assertTrue(provider instanceof Parameter);
    }

    @Test
    public void newParameter1() {
        ParameterProvider provider = ParameterKit.newParameter("p1", "124");
        Assert.assertEquals("p1", provider.getName());
        Assert.assertEquals("124", provider.getValue());
    }
}