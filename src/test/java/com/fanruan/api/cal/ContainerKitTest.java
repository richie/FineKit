package com.fanruan.api.cal;

import com.fr.stable.ArrayProvider;
import com.fr.third.guava.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-29
 */
public class ContainerKitTest {

    @Test
    public void newArray() {

        ArrayProvider<String> array = ContainerKit.newArray(new String[]{"abc", "xyz"});
        Assert.assertEquals("xyz", array.elementAt(1));

        ArrayProvider<String> array2 = ContainerKit.newArray(Lists.newArrayList("abc", "xyz"));
        Assert.assertEquals("abc", array2.elementAt(0));
    }
}