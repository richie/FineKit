package com.fanruan.api.cal;

import com.fr.general.FRLogger;
import com.fr.stable.script.CalculatorProvider;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 */
public class CalculatorKitTest {
    @Test
    public void testCalculatorCreate() {
        CalculatorProvider calculator = CalculatorKit.createCalculator();
        try {
            Assert.assertEquals(3, calculator.evalValue("sum(1,2)"));
        } catch (Exception e) {
            FRLogger.getLogger().error(e.getMessage(), e);
        }
    }

    @Test
    public void testCalculatorCreateWithPara() {
        Map paraMap = new HashMap();
        paraMap.put("a", 1);
        paraMap.put("b", 2);
        CalculatorProvider calculator = CalculatorKit.createCalculator(paraMap);
        try {
            Assert.assertEquals(3, calculator.evalValue("sum(a,b)"));
        } catch (Exception e) {
            FRLogger.getLogger().error(e.getMessage(), e);
        }
    }
}