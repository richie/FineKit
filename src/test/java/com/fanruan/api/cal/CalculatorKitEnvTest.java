package com.fanruan.api.cal;

import com.fanruan.api.Prepare;
import com.fr.invoke.Reflect;
import com.fr.log.FineLoggerFactory;
import com.fr.main.impl.WorkBook;
import com.fr.main.workbook.ResultWorkBook;
import com.fr.report.cell.CellElementValueConverterRegistry;
import com.fr.stable.ActorConstants;
import com.fr.stable.ActorFactory;
import com.fr.web.core.ReportSessionIDInfor;
import com.fr.web.core.SessionPoolManager;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 需要启动模块的算子单元测试
 *
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 */
public class CalculatorKitEnvTest extends Prepare {
    @Test
    public void testSheetInterval() {
        WorkBook tpl = new WorkBook();
        Map paraMap = new HashMap(0);
        String path = "/com/fanruan/api/report/sheets.cpt";
        try {
            tpl.readStream(CalculatorKitEnvTest.class.getResourceAsStream(path));
            ResultWorkBook resultWorkBook = tpl.execute(paraMap, ActorFactory.getActor(ActorConstants.TYPE_WRITE));
            ReportSessionIDInfor sessionIDInfor = new ReportSessionIDInfor(paraMap, path, ActorFactory.getActor(ActorConstants.TYPE_WRITE)).buildResultWorkBook(resultWorkBook);
            String sessionId = Reflect.on(SessionPoolManager.class).call("addSessionIDInfor", sessionIDInfor).get();
            CellElementValueConverterRegistry.complete();
            Assert.assertEquals(111, CalculatorKit.createCalculator(sessionId, null).evalValue("sheet1!A1"));
            Assert.assertEquals(222, CalculatorKit.createCalculator(sessionId, null).evalValue("sheet2!A1"));
            Assert.assertEquals(333, CalculatorKit.createCalculator(sessionId, null).evalValue("SUM(sheet1!A1,sheet2!A1)"));
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        } finally {
            CellElementValueConverterRegistry.reset();
        }
        Assert.assertNotNull(tpl.getReport(0));
    }
}