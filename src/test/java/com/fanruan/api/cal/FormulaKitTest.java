package com.fanruan.api.cal;

import com.fanruan.api.Prepare;
import com.fanruan.api.err.KitError;
import com.fr.base.BaseFormula;
import com.fr.base.Formula;
import com.fr.base.ParameterMapNameSpace;
import com.fr.script.Calculator;
import com.fr.stable.UtilEvalError;
import com.fr.stable.script.CalculatorProvider;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-15
 */
public class FormulaKitTest extends Prepare {

    @Test
    public void eval() {
        try {
            Assert.assertEquals(4, FormulaKit.eval("=(6+6)/3"));
        } catch (KitError kitError) {
            Assert.fail();
        }
    }

    @Test
    public void testEval() {
        CalculatorProvider calculator = Calculator.createCalculator();
        Map<String, Object> map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        calculator.pushNameSpace(ParameterMapNameSpace.create(map));
        try {
            Assert.assertEquals(1, FormulaKit.eval(calculator,"=($a+$b)/3"));
        } catch (KitError kitError) {
            Assert.fail();
        }
    }

    @Test
    public void testScriptFormula() {
        BaseFormula formula = FormulaKit.newScriptFormula("=return Math.abs(-1)");
        CalculatorProvider provider = CalculatorKit.createCalculator();
        try {
            Object r = formula.evalValue(provider);
            Assert.assertEquals(1, r);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCheckFormulaContent() {
        assertEquals(true, FormulaKit.checkFormulaContent(new Formula("")));
        assertEquals(true, FormulaKit.checkFormulaContent(new Formula("SUM(1,1)")));


        assertEquals(true, FormulaKit.checkFormulaContent(""));
        assertEquals(true, FormulaKit.checkFormulaContent(("=SUM(1,1)")));
        assertEquals(true, FormulaKit.checkFormulaContent(("SUM(1,1)")));


        assertEquals(true, FormulaKit.checkFormulaContent(new Formula("=")));
        assertEquals(false, FormulaKit.checkFormulaContent("="));
    }
}