package com.fanruan.api.log;

import com.fanruan.api.Prepare;
import com.fr.general.FRLogger;
import com.fr.general.log.Log4jConfig;
import com.fr.log.FineLoggerFactory;
import com.fr.third.apache.log4j.Level;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 */
public class LogKitTest extends Prepare {

    @Test
    public void isDebugEnabled() {
        Log4jConfig.getInstance().setRootLevel(Level.DEBUG);
        FineLoggerFactory.registerLogger(FRLogger.getLogger());
        Assert.assertTrue(LogKit.isDebugEnabled());
        Assert.assertTrue(LogKit.isInfoEnabled());
    }

}