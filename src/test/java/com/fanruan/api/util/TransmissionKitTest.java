package com.fanruan.api.util;

import com.fanruan.api.util.trans.BatchSmsBody;
import com.fanruan.api.util.trans.SingleSmsBody;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 */
public class TransmissionKitTest {
    @Test
    public void testSmsBody() {
        try {
            SingleSmsBody.newBuilder().mobile("").para(null).build();
            BatchSmsBody.newBuilder().mobiles(null).build();
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }
}