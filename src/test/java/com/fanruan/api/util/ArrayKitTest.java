package com.fanruan.api.util;

import com.fanruan.api.Prepare;
import org.junit.Assert;
import org.junit.Test;

public class ArrayKitTest extends Prepare {
    @Test
    public void isEmpty() throws Exception{
        Object[] emptyArray = new Object[0];
        Object[] notEmptyArray = new Object[1];
        Assert.assertTrue(ArrayKit.isEmpty(emptyArray));
        Assert.assertFalse(ArrayKit.isEmpty(notEmptyArray));

    }

    @Test
    public void remove() throws Exception{
        Integer[] intTestArray = {1, 2, 3, 4};
        Integer[] intTestResult = {1, 2, 3};
        Boolean[] booleanTestArray = {true, true, true, false};
        Boolean[] booleanTestResult = {true, true, true};
        Byte[] byteTestArray = {100, 101, 102, 103};
        Byte[] byteTestResult = {100, 101, 102};
        Character[] charTestArray = {'a', 'b', 'c', 'd'};
        Character[] charTestResult = {'a', 'b', 'c'};
        Double[] doubleTestArray = {1.1, 2.2, 3.3, 4.4};
        Double[] doubleTestResult = {1.1, 2.2, 3.3};
        Float[] floatTestArray = {1.1f, 2.2f, 3.3f, 4.4f};
        Float[] floatTestResult = {1.1f, 2.2f, 3.3f};
        Long[] longTestArray = {1L, 2L, 3L, 4L};
        Long[] longTestResult = {1L, 2L, 3L};
        Short[] shortTestArray = {1, 2, 3, 4};
        Short[] shortTestResult = {1, 2, 3};
        Assert.assertEquals(ArrayKit.remove(intTestArray, 3), intTestResult);
        Assert.assertEquals(ArrayKit.remove(booleanTestArray, 3), booleanTestResult);
        Assert.assertEquals(ArrayKit.remove(byteTestArray, 3), byteTestResult);
        Assert.assertEquals(ArrayKit.remove(charTestArray, 3), charTestResult);
        Assert.assertEquals(ArrayKit.remove(doubleTestArray, 3), doubleTestResult);
        Assert.assertEquals(ArrayKit.remove(floatTestArray, 3), floatTestResult);
        Assert.assertEquals(ArrayKit.remove(longTestArray, 3), longTestResult);
        Assert.assertEquals(ArrayKit.remove(shortTestArray, 3), shortTestResult);
    }

    @Test
    public void add() throws Exception{
        Integer[] intTestArray = {1, 2, 3, 4};
        Integer[] intTestResult = {1, 2, 3, 4, 5};
        Boolean[] booleanTestArray = {true, true, true, false};
        Boolean[] booleanTestResult = {true, true, true, false, true};
        Byte[] byteTestArray = {1};
        Byte[] byteTestResult = {1, 0};
        Character[] charTestArray = {'a', 'b', 'c', 'd'};
        Character[] charTestResult = {'a', 'b', 'c', 'd', 'e'};
        Double[] doubleTestArray = {1.1, 2.2, 3.3, 4.4};
        Double[] doubleTestResult = {1.1, 2.2, 3.3, 4.4, 5.5};
        Float[] floatTestArray = {1.1f, 2.2f, 3.3f, 4.4f};
        Float[] floatTestResult = {1.1f, 2.2f, 3.3f, 4.4f, 5.5f};
        Long[] longTestArray = {1L, 2L, 3L, 4L};
        Long[] longTestResult = {1L, 2L, 3L, 4L, 5L};
        Short[] shortTestArray = {1, 2, 3, 4};
        Short[] shortTestResult = {1, 2, 3, 4, 5};
        Assert.assertEquals(ArrayKit.add(intTestArray, 5), intTestResult);
        Assert.assertEquals(ArrayKit.add(booleanTestArray, true), booleanTestResult);
        Assert.assertEquals(ArrayKit.add(byteTestArray, new Byte("0")), byteTestResult);
        Assert.assertEquals(ArrayKit.add(charTestArray, 'e'), charTestResult);
        Assert.assertEquals(ArrayKit.add(doubleTestArray, 5.5), doubleTestResult);
        Assert.assertEquals(ArrayKit.add(floatTestArray, 5.5f), floatTestResult);
        Assert.assertEquals(ArrayKit.add(longTestArray, 5L), longTestResult);
        Assert.assertEquals(ArrayKit.add(shortTestArray, new Short("5")), shortTestResult);
    }

    @Test
    public void addAll() throws Exception{
        Integer[] intTestArray = {1, 2, 3, 4};
        Integer[] intTestResult = {1, 2, 3, 4, 5, 6};
        Boolean[] booleanTestArray = {true, true, true, false};
        Boolean[] booleanTestResult = {true, true, true, false, true, true};
        Byte[] byteTestArray = {1};
        Byte[] byteTestResult = {1, 0, 0};
        Character[] charTestArray = {'a', 'b', 'c', 'd'};
        Character[] charTestResult = {'a', 'b', 'c', 'd', 'e', 'f'};
        Double[] doubleTestArray = {1.1, 2.2, 3.3, 4.4};
        Double[] doubleTestResult = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6};
        Float[] floatTestArray = {1.1f, 2.2f, 3.3f, 4.4f};
        Float[] floatTestResult = {1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f};
        Long[] longTestArray = {1L, 2L, 3L, 4L};
        Long[] longTestResult = {1L, 2L, 3L, 4L, 5L, 6L};
        Short[] shortTestArray = {1, 2, 3, 4};
        Short[] shortTestResult = {1, 2, 3, 4, 5, 6};
        Assert.assertEquals(ArrayKit.addAll(intTestArray, 5,6), intTestResult);
        Assert.assertEquals(ArrayKit.addAll(booleanTestArray, true, true), booleanTestResult);
        Assert.assertEquals(ArrayKit.addAll(byteTestArray, new Byte("0"), new Byte("0")), byteTestResult);
        Assert.assertEquals(ArrayKit.addAll(charTestArray, 'e','f'), charTestResult);
        Assert.assertEquals(ArrayKit.addAll(doubleTestArray, 5.5,6.6), doubleTestResult);
        Assert.assertEquals(ArrayKit.addAll(floatTestArray, 5.5f,6.6f), floatTestResult);
        Assert.assertEquals(ArrayKit.addAll(longTestArray, 5L,6L), longTestResult);
        Assert.assertEquals(ArrayKit.addAll(shortTestArray, new Short("5"),new Short("6")), shortTestResult);
    }
}
