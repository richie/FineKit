package com.fanruan.api.util;

import com.fanruan.api.Prepare;
import com.fr.general.GeneralUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class GeneralKitTest extends Prepare {

    @Test
    public void getMacAddress() throws Exception{
        Assert.assertEquals(GeneralKit.getMacAddress(), com.fr.general.GeneralUtils.getMacAddress());
    }

    @Test
    public void objectToString() {
        Integer s = 1;
        Assert.assertEquals(GeneralKit.objectToString(s),"1");
    }

    @Test
    public void readBuildNO() {
        Assert.assertEquals(GeneralKit.readBuildNO(), GeneralUtils.readBuildNO());
    }


    @Test
    public void compare() {
        Integer s = 1;
        Integer s3 = 1;
        Integer s1 = 2;
        Integer s2 = 0;
        Assert.assertEquals(GeneralKit.compare(s,s1),-1);
        Assert.assertEquals(GeneralKit.compare(s,s3),0);
        Assert.assertEquals(GeneralKit.compare(s,s2),1);
    }
}