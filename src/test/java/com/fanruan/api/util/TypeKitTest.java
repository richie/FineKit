package com.fanruan.api.util;

import org.junit.Assert;
import org.junit.Test;

public class TypeKitTest {

    @Test
    public void classInstanceOf() {
        Assert.assertTrue(TypeKit.classInstanceOf(Integer.class, Object.class));
        Assert.assertFalse(TypeKit.classInstanceOf(Object.class, Integer.class));
    }

    @Test
    public void objectInstanceOf() {
        Assert.assertTrue(TypeKit.objectInstanceOf(1, Integer.class));
        Assert.assertFalse(TypeKit.objectInstanceOf(1.0, Integer.class));
    }
}