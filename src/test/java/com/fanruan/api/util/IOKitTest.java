package com.fanruan.api.util;

import com.fanruan.api.Prepare;
import org.junit.Assert;
import org.junit.Test;
import sun.nio.cs.ext.GBK;

import com.fr.general.IOUtils;
import javax.swing.*;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 */
public class IOKitTest extends Prepare {

    @Test
    public void testEncode() {
        Assert.assertEquals("UTF-8", StandardCharsets.UTF_8.toString());
    }

    @Test
    public void testReadResourceAsString() {
        String text = IOKit.readResourceAsString("/com/fanruan/api/util/iokit.txt");
        Assert.assertEquals("Hello World!", text);
    }

    @Test
    public void readIcon() {
        Assert.assertEquals((IOKit.readIcon("/com/fanruan/api/util/close.png")).getIconHeight(),(new ImageIcon(IOUtils.readImageWithCache("/com/fanruan/api/util/close.png")).getIconHeight()));
    }

    @Test
    public void readResource() {
        try {
            InputStream in = IOKit.readResource("/com/fanruan/api/util/iokit.txt");
            StringBuffer out = new StringBuffer();
            byte[] b = new byte[4096];
            for (int n; (n = in.read(b)) != -1; ) {
                out.append(new String(b, 0, n));
            }
            Assert.assertEquals("Hello World!", out.toString());
        } catch (Exception e) {

        }
    }
}