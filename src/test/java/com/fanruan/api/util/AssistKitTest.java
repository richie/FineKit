package com.fanruan.api.util;

import com.fr.event.Null;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Objects;

import static org.junit.Assert.*;

public class AssistKitTest {

    @Test
    public void equals1() {
        double a = 1.1;
        double b = 1.1;
        double c = 1.11;
        Assert.assertTrue(AssistKit.equals(a, b));
        Assert.assertFalse(AssistKit.equals(b, c));
    }

    @Test
    public void equals2() {
        float a = 1.1f;
        float b = 1.1f;
        float c = 1.111f;
        Assert.assertTrue(AssistKit.equals(a, b));
        Assert.assertFalse(AssistKit.equals(b, c));
    }

    @Test
    public void equals3() {
        Object a = 126;
        Object b = 126;
        Object c = new Integer(256);
        Object d = new Integer(256);
        Object e = 111.1f;
        Assert.assertTrue(AssistKit.equals(a, b));
        Assert.assertTrue(AssistKit.equals(null, null));
        Assert.assertFalse(AssistKit.equals(null, a));
        Assert.assertFalse(AssistKit.equals(a,e));

    }

    @Test
    public void compare() {
        Assert.assertEquals(0, AssistKit.compare(1, 1));
        Assert.assertEquals(1, AssistKit.compare(2, 1));
        Assert.assertEquals(-1, AssistKit.compare(1, 2));
    }

    @Test
    public void compare1() {
        Assert.assertEquals(0, AssistKit.compare(1l, 1l));
        Assert.assertEquals(1, AssistKit.compare(2l, 1l));
        Assert.assertEquals(-1, AssistKit.compare(1l, 2l));
    }

    @Test
    public void hashCode1() {
        String s = "hello";
        Object[] a = new Object[]{s};
        Assert.assertEquals(Arrays.hashCode(a), AssistKit.hashCode(s));
        Assert.assertEquals(0, AssistKit.hashCode(null));
    }
}