package com.fanruan.api.util;

import com.fanruan.api.Prepare;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-27
 */
public class RenderKitTest extends Prepare {

    @Test
    public void renderParameter4Tpl() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("age", 20);
        map.put("name", "Alex");
        map.put("salary", 100);
        String text = "${name} is ${age} years old, he earned ${salary} dollars per month.";
        Assert.assertEquals("Alex is 20 years old, he earned 100 dollars per month.", RenderKit.renderParameter4Tpl(text, map));
    }
}