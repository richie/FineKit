package com.fanruan.api.util;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class StringKitTest {

    @Test
    public void isEmpty() {
        assertEquals(StringKit.isEmpty(""),true);
        assertEquals(StringKit.isEmpty("asd"),false);
    }

    @Test
    public void isNotEmpty() {
        assertEquals(StringKit.isNotEmpty("asd"),true);
        assertEquals(StringKit.isNotEmpty(""),false);
    }

    @Test
    public void isBlank() {
        assertEquals(StringKit.isNotBlank(null),false);
        assertEquals(StringKit.isNotBlank(""),false);
        assertEquals(StringKit.isNotBlank(" "),false);
        assertEquals(StringKit.isNotBlank("bob"),true);
        assertEquals(StringKit.isNotBlank("  bob  "),true);
    }

    @Test
    public void isNotBlank() {
        assertEquals(StringKit.isBlank(null),true);
        assertEquals(StringKit.isBlank(""),true);
        assertEquals(StringKit.isBlank(" "),true);
        assertEquals(StringKit.isBlank("bob"),false);
        assertEquals(StringKit.isBlank("  bob  "),false);
    }
    @Test
    public void trim(){
        String stringHaveBlank = "  abc  ";
        String stringWithoutBlank = "abc";
        String blankString = "   ";
        Assert.assertEquals(StringKit.trim(stringHaveBlank), "abc");
        Assert.assertEquals(StringKit.trim(stringWithoutBlank), "abc");
        Assert.assertEquals(StringKit.trim(blankString), "");
    }

    @Test
    public void alwaysNotNull() {
        assertEquals(StringKit.alwaysNotNull(null),"");
        assertEquals(StringKit.alwaysNotNull(""),"");
        assertEquals(StringKit.alwaysNotNull("asd"),"asd");
    }

    @Test
    public void cutStringEndWith() {
        assertEquals(StringKit.cutStringEndWith("bob is","is"), "bob ");
        assertEquals(StringKit.cutStringEndWith("","is"), "");
    }

    @Test
    public void cutStringStartWith() {
        assertEquals(StringKit.cutStringStartWith("bob is","bob "), "is");
        assertEquals(StringKit.cutStringStartWith("","is"), "");
    }

    @Test
    public void trimToNull() {
        assertEquals(StringKit.trimToNull("    "),null);
        assertEquals(StringKit.trimToNull(" s  "),"s");
    }

    @Test
    public void perfectStart() {
        assertEquals(StringKit.perfectStart("bobob","bob"),"bobob");
        assertEquals(StringKit.perfectStart("sbobob","bob"),"bobsbobob");
    }

    @Test
    public void perfectEnd() {
        assertEquals(StringKit.perfectEnd("bobob","bob"),"bobob");
        assertEquals(StringKit.perfectEnd("bobobs","bob"),"bobobsbob");
    }

    @Test
    public void perfectSurround() {
        assertEquals(StringKit.perfectSurround("bobob","bob"),"bobob");
        assertEquals(StringKit.perfectSurround("sbobobs","bob"),"bobsbobobsbob");
    }

    @Test
    public void getLength() {
        assertEquals(StringKit.getLength("asd"),3);
        assertEquals(StringKit.getLength(""),0);
        assertEquals(StringKit.getLength(null),0);
    }

    @Test
    public void equalsIgnore() {
        assertEquals(StringKit.equalsIgnore("asd","asd","qwewqe"),true);
        assertEquals(StringKit.equalsIgnore("asd","as","d"),true);
        assertEquals(StringKit.equalsIgnore("asd","asdd","d"),false);
    }

    @Test
    public void join() {
        String[] s = new String[3];
        String[] s1 = new String[0];
        s[0]= new String("qqq");
        s[1]= new String("www");
        s[2]= new String("eee");
        assertEquals(StringKit.join("asd",s),"qqqasdwwwasdeee");
        assertEquals(StringKit.join("",s),"qqqwwweee");
        assertEquals(StringKit.join("asd",null),null);
        assertEquals(StringKit.join("asd",s1),"");
    }


    @Test
    public void parseVersion() {
        assertEquals(StringKit.parseVersion("BCD123"),"123123");
    }

    @Test
    public void isArrayType() {
        assertEquals(StringKit.isArrayType("[[]]"),true);
        assertEquals(StringKit.isArrayType("[["),false);
    }

    @Test
    public void stringToArray() {
        String[][] s = new String[1][3];
        s[0][0] = new String("a");
        s[0][1] = new String("b");
        s[0][2] = new String("c");
        assertEquals(StringKit.stringToArray("[[a,b,c]]"),s);
    }

    @Test
    public void subStringByByteLength() {
        try {
            assertEquals(StringKit.subStringByByteLength("asd", "UTF-8", 1), "a");
            assertEquals(StringKit.subStringByByteLength("  ", "UTF-8", 1), "");
            assertEquals(StringKit.subStringByByteLength("asd", "UTF-8", 0), "");
        }catch (Exception e){

        }
    }

    @Test
    public void equals() {
        String s = new String("asd");
        String s1 = new String("asd");
        String s2 = new String("asds");
        assertEquals(StringKit.equals(s, s1), true);
        assertEquals(StringKit.equals(s, s2), false);
        assertEquals(StringKit.equals(null, null), true);
    }

    @Test
    public void equalsIgnoreCase() {
        String s = new String("Asd");
        String s1 = new String("asd");
        String s2 = new String("asds");
        assertEquals(StringKit.equalsIgnoreCase(s, s1), true);
        assertEquals(StringKit.equalsIgnoreCase(s, s2), false);
        assertEquals(StringKit.equalsIgnoreCase(null, null), true);
    }

    @Test
    public void rightPad() {
        assertEquals(StringKit.rightPad("asd",5), "asd  ");
        assertEquals(StringKit.rightPad("asd",-1), "asd");
    }
}