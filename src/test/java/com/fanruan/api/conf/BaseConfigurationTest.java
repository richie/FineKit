package com.fanruan.api.conf;

import com.fanruan.api.Prepare;
import com.fr.config.holder.Conf;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 */
public class BaseConfigurationTest extends Prepare {

    @Test
    public void testGet() {
        MyConf conf = MyConf.getInstance();
        Assert.assertEquals(10, conf.getCount());
        conf.setCount(100);
        Assert.assertEquals(100, conf.getCount());
    }

    private static class MyConf extends BaseConfiguration {

        private static volatile MyConf instance = null;

        public static MyConf getInstance() {
            if (instance == null) {
                instance = getConfigInstance(MyConf.class);
            }
            return instance;
        }

        private Conf<Integer> count = HolderKit.simple(10);

        public int getCount() {
            return count.get();
        }

        public void setCount(int count) {
            this.count.set(count);
        }
    }
}