package com.fanruan.api.conf.xml;

import com.fr.config.holder.Conf;
import com.fr.config.holder.impl.xml.XmlConf;
import com.fr.stable.xml.XMLable;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 * 将xml文件写入fine_conf_entity的帮助类，不建议使用
 */
public class XmlHolderKit {

    /**
     * 创建一个要写入配置的xml多项
     * @param t 配置对象
     * @param clazz 类型
     * @param xmlTag xml标签
     * @param <T> 类型
     * @return 配置对象
     */
    public static <T extends XMLable> Conf<T> obj(T t, Class<T> clazz, String xmlTag) {
        return new XmlConf(t, clazz, xmlTag);
    }
}
