package com.fanruan.api.conf;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 */
public interface Runner {

    void run();
}
