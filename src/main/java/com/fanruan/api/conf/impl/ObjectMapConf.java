package com.fanruan.api.conf.impl;

import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class ObjectMapConf<T extends Map> extends com.fr.config.holder.impl.ObjectMapConf<Map> {
    public ObjectMapConf(String property, Map map, Class keyType, Class valueType) {
        super(property, map, keyType, valueType);
    }

    public ObjectMapConf(String property, Map map, Class keyType, Class valueType, boolean order) {
        super(property, map, keyType, valueType, order);
    }

    public ObjectMapConf(Map map, Class keyType, Class valueType, boolean order) {
        super(map, keyType, valueType, order);
    }

    public ObjectMapConf(Map map, Class keyType, Class valueType) {
        super(map, keyType, valueType);
    }
}
