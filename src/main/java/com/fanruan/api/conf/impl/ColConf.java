package com.fanruan.api.conf.impl;

import java.util.Collection;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class ColConf<T extends Collection> extends com.fr.config.holder.impl.ColConf<Collection> {

    public ColConf(String property, Collection collection, Class valueType) {
        super(property, collection, valueType);
    }

    public ColConf(Collection collection, Class valueType) {
        super(collection, valueType);
    }
}
