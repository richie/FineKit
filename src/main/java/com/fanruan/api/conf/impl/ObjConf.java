package com.fanruan.api.conf.impl;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class ObjConf<T> extends com.fr.config.holder.impl.ObjConf<T> {
    public ObjConf(String property, T t, Class classType) {
        super(property, t, classType);
    }

    public ObjConf(T t, Class classType) {
        super(t, classType);
    }
}
