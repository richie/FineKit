package com.fanruan.api.conf.impl;

import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class MapConf<T extends Map> extends com.fr.config.holder.impl.MapConf<Map> {

    public MapConf(String property, Map map, Class keyType, Class valueType) {
        super(property, map, keyType, valueType);
    }

    public MapConf(Map map, Class keyType, Class valueType) {
        super(map, keyType, valueType);
    }
}
