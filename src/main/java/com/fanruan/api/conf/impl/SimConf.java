package com.fanruan.api.conf.impl;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class SimConf<T> extends com.fr.config.holder.impl.SimConf<T> {

    public SimConf(T t) {
        super(t);
    }

    public SimConf(String property, T t) {
        super(property, t);
    }
}
