package com.fanruan.api.conf.impl;

import java.util.Collection;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 */
public class ObjectColConf<T extends Collection>  extends com.fr.config.holder.impl.ObjectColConf<Collection> {
    public ObjectColConf(String property, Collection collection, Class valueType) {
        super(property, collection, valueType);
    }

    public ObjectColConf(Collection collection, Class valueType) {
        super(collection, valueType);
    }

    public ObjectColConf(Collection collection, Class valueType, boolean order) {
        super(collection, valueType, order);
    }
}
