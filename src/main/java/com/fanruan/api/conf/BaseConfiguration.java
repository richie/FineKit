package com.fanruan.api.conf;

import com.fr.config.ConfigContext;
import com.fr.config.DefaultConfiguration;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 * 插件要实现配置类的时候，直接继承该抽象类即可
 * <p>
 *     private static class MyConf extends BaseConfiguration {
 *
 *         private static volatile MyConf instance = null;
 *
 *         public static MyConf getInstance() {
 *             if (instance == null) {
 *                 instance = getConfigInstance(MyConf.class);
 *             }
 *             return instance;
 *         }
 *     }
 * <p/>
 */
public abstract class BaseConfiguration extends DefaultConfiguration {

    /**
     * 从配置对象池中创建或者获取配置对象实例
     * @param clazz 配置实例类型
     * @param <T> 类型
     * @return 配置对象
     */
    public static <T extends BaseConfiguration> T getConfigInstance(final Class<T> clazz) {
        return ConfigContext.getConfigInstance(clazz);
    }
}
