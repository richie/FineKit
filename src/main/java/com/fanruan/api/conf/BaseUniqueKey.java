package com.fanruan.api.conf;

import com.fr.config.utils.UniqueKey;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 * 需要存入fine_conf_entity配置库中的属性对象，需要继承此抽象类
 */
public abstract class BaseUniqueKey extends UniqueKey {
}
