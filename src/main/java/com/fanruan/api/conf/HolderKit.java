package com.fanruan.api.conf;

import com.fanruan.api.conf.impl.ObjConf;
import com.fanruan.api.conf.impl.ObjectColConf;
import com.fanruan.api.conf.impl.ObjectMapConf;
import com.fanruan.api.conf.impl.SimConf;
import com.fr.config.holder.Conf;

import java.util.Collection;
import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-15
 * 配置对象操作类
 * TODO:补充单元测试
 */
public class HolderKit {

    /**
     * 初始化一个基本对象类型的配置
     * @param t 初始值
     * @param <T> 类型
     * @return 配置对象
     */
    public static <T> Conf<T> simple(T t) {
        return new SimConf<>(t);
    }

    /**
     * 初始化一个对象类型的配置
     * @param t 初始值
     * @param type 对象class类
     * @param <T> 类型
     * @return 配置对象
     */
    public static <T> Conf<T> obj(T t, Class<T> type) {
        return new ObjConf<>(t, type);
    }

    /**
     * 初始化一个集合类型的配置
     * @param collection 初始值
     * @param type 对象class类型
     * @param <T> 类型
     * @return 配置对象
     */
    public static <T> ObjectColConf<Collection<T>> objCollection(Collection<T> collection, Class<T> type) {
        return new ObjectColConf<>(collection, type);
    }

    /**
     * 初始化一个集合类型的配置
     * @param collection 初始值
     * @param type 对象class类型
     * @param order 是否是有序的配置
     * @param <T> 类型
     * @return 配置对象
     */
    public static <T> ObjectColConf<Collection<T>> objCollection(Collection<T> collection, Class<T> type, boolean order) {
        return new ObjectColConf<>(collection, type, order);
    }

    /**
     * 初始化一个字典类型的配置
     * @param map 初始值
     * @param keyType 键class类型
     * @param valueType 值class类型
     * @param <K> 键类型
     * @param <V> 值类型
     * @return 配置对象
     */
    public static <K, V> ObjectMapConf<Map<K, V>> objMap(Map<K, V> map, Class<K> keyType, Class<V> valueType) {
        return new ObjectMapConf<>(map, keyType, valueType);
    }

    /**
     * 初始化一个字典类型的配置
     * @param map 初始值
     * @param keyType 键class类型
     * @param valueType 值class类型
     * @param order 是否是有序配置
     * @param <K> 键类型
     * @param <V> 值类型
     * @return 配置对象
     */
    public static <K, V> ObjectMapConf<Map<K, V>> objMap(Map<K, V> map, Class<K> keyType, Class<V> valueType, boolean order) {
        return new ObjectMapConf<>(map, keyType, valueType, order);
    }
}
