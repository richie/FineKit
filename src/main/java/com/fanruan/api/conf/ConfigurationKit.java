package com.fanruan.api.conf;

import com.fr.config.Configuration;
import com.fr.config.holder.ConfigChangeListener;
import com.fr.transaction.Configurations;
import com.fr.transaction.ValidateProxy;
import com.fr.transaction.WorkerFacade;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 * 配置监听管理器
 */
public class ConfigurationKit {

    /**
     * 注册监听配置变化的监听器
     *
     * @param listener 监听器
     */
    public static void registerListener(ConfigChangeListener listener) {
        ValidateProxy.getInstance().getValidateManager().registerListener(listener);
    }

    /**
     * 保存配置
     *
     * @param type   配置的类型
     * @param action 保存动作
     */
    public static void modify(Class<? extends Configuration> type, Runner action) {
        Configurations.modify(new WorkerFacade(type) {
            @Override
            public void run() {
                action.run();
            }
        });
    }

}
