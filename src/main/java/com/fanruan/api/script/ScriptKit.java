package com.fanruan.api.script;
import com.fr.script.ScriptFactory;

import javax.script.ScriptEngine;

public class ScriptKit {
    /**
     * 获取一个全新的脚本执行引擎
     * @return 一个全新的脚本执行引擎
     */
    public static ScriptEngine newScriptEngine() {
        return ScriptFactory.newScriptEngine();
    }
}
