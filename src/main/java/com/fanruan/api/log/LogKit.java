package com.fanruan.api.log;

import com.fr.log.FineLoggerFactory;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 * 写日志的工具类
 */
public class LogKit {

    /**
     * 输出SQL日志，在日志中会有前缀[SQL]
     *
     * @param sql sql内容
     */
    public static void sql(String sql) {
        FineLoggerFactory.getLogger().sql(sql);
    }

    /**
     * 是否启用了debug级别的日志输出
     *
     * @return 启用了debug级别的日志输出则返回true，否则返回false
     */
    public static boolean isDebugEnabled() {
        return FineLoggerFactory.getLogger().isDebugEnabled();
    }

    /**
     * 输出debug级别的日志信息
     *
     * @param msg 信息
     */
    public static void debug(String msg) {
        FineLoggerFactory.getLogger().debug(msg);
    }

    /**
     * 输出debug级别的日志信息
     *
     * @param format 日志信息模板，形如: "hello, LiLei, I am {}."
     * @param args 模板中的参数，和{}出现的顺序对应
     */
    public static void debug(String format, Object... args) {
        FineLoggerFactory.getLogger().debug(format, args);
    }

    /**
     * 输出debug级别的日志信息
     * @param msg 日志信息
     * @param e 异常对象
     */
    public static void debug(String msg, Throwable e) {
        FineLoggerFactory.getLogger().debug(msg, e);
    }

    /**
     * 是否启用了info级别的日志输出
     *
     * @return 启用了info级别的日志输出则返回true，否则返回false
     */
    public static boolean isInfoEnabled() {
        return FineLoggerFactory.getLogger().isInfoEnabled();
    }

    /**
     * 输出info级别的日志信息
     *
     * @param msg 信息
     */
    public static void info(String msg) {
        FineLoggerFactory.getLogger().info(msg);
    }

    /**
     * 输出info级别的日志信息
     *
     * @param format 日志信息模板，形如: "hello, LiLei, I am {}."
     * @param args 模板中的参数，和{}出现的顺序对应
     */
    public static void info(String format, Object... args) {
        FineLoggerFactory.getLogger().info(format, args);
    }

    /**
     * 输出warn级别的日志信息
     *
     * @param msg 信息
     */
    public static void warn(String msg) {
        FineLoggerFactory.getLogger().warn(msg);
    }

    /**
     * 输出warn级别的日志信息
     *
     * @param format 日志信息模板，形如: "hello, LiLei, I am {}."
     * @param args 模板中的参数，和{}出现的顺序对应
     */
    public static void warn(String format, Object... args) {
        FineLoggerFactory.getLogger().warn(format, args);
    }

    /**
     * 输出warn级别的日志信息
     * @param msg 日志信息
     * @param e 异常对象
     */
    public static void warn(String msg, Throwable e) {
        FineLoggerFactory.getLogger().warn(msg, e);
    }

    /**
     * 输出warn级别的日志信息
     * @param e 异常对象
     * @param format 日志信息模板，形如: "hello, LiLei, I am {}."
     * @param args 模板中的参数，和{}出现的顺序对应
     */
    public static void warn(Throwable e, String format, Object... args) {
        FineLoggerFactory.getLogger().warn(e, format, args);
    }

    /**
     * 输出error级别信息
     *
     * @param msg 信息
     */
    public static void error(String msg) {
        FineLoggerFactory.getLogger().error(msg);
    }

    /**
     * 输出error级别的日志信息
     *
     * @param format 日志信息模板，形如: "hello, LiLei, I am {}."
     * @param args 模板中的参数，和{}出现的顺序对应
     */
    public static void error(String format, Object... args) {
        FineLoggerFactory.getLogger().error(format, args);
    }

    /**
     * 输出error级别信息
     *
     * @param msg 信息
     * @param e   异常
     */
    public static void error(String msg, Throwable e) {
        FineLoggerFactory.getLogger().error(msg, e);
    }

    /**
     * 输出error级别的日志信息
     * @param e 异常对象
     * @param format 日志信息模板，形如: "hello, LiLei, I am {}."
     * @param args 模板中的参数，和{}出现的顺序对应
     */
    public static void error(Throwable e, String format, Object... args) {
        FineLoggerFactory.getLogger().error(e, format, args);
    }
}
