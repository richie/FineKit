package com.fanruan.api.login;

import com.fr.decision.authorize.Passport;
import com.fr.decision.authorize.impl.HttpPassport;
import com.fr.decision.config.FSConfig;
import org.jetbrains.annotations.Nullable;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 */
public class LoginKit {

    /**
     * 获取决策平台设置的认证类型
     * @return 认证对象
     */
    public static Passport getCurrentPassport() {
        return FSConfig.getInstance().getPassport();
    }

    /**
     * 获取http认证的地址
     * @return http认证地址
     */
    public static @Nullable String getHttpPassportUrl() {
        Passport passport = getCurrentPassport();
        if (passport instanceof HttpPassport) {
            return ((HttpPassport) passport).getUrl();
        }
        return null;
    }

    /**
     * 获取http认证的秘钥
     * @return http认证地址
     */
    public static @Nullable String getHttpPassportKey() {
        Passport passport = getCurrentPassport();
        if (passport instanceof HttpPassport) {
            return ((HttpPassport) passport).getPublicKey();
        }
        return null;
    }
}
