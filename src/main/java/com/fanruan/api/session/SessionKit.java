package com.fanruan.api.session;

import com.fr.stable.web.SessionProvider;
import com.fr.web.core.SessionPoolManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class SessionKit {

    /**
     * 根据sessionID信息返回会话对象
     * @param sessionID 会话唯一标识符
     * @return 会话对象
     */
    public static @Nullable SessionProvider getSession(@NotNull String sessionID) {
        return SessionPoolManager.getSessionIDInfor(sessionID, SessionProvider.class);
    }
}
