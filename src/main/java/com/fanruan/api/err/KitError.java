package com.fanruan.api.err;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-15
 */
public class KitError extends Exception {

    public KitError() {
        super();
    }

    public KitError(String message) {
        super(message);
    }

    public KitError(Throwable throwable) {
        super(throwable);
    }
}
