package com.fanruan.api.err;

/**
 * 公式计算异常
 */
public class UtilEvalError extends com.fr.stable.UtilEvalError {

    public UtilEvalError() {

    }

    public UtilEvalError(String message) {
        super(message);
    }
}
