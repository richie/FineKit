package com.fanruan.api.cal.namespace;

import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.main.FineBook;
import com.fr.parser.SheetIntervalLiteral;
import com.fr.report.report.Report;
import com.fr.report.report.WriteECReport;
import com.fr.stable.StringUtils;
import com.fr.stable.script.AbstractNameSpace;
import com.fr.stable.script.CalculatorProvider;
import com.fr.stable.script.ColumnRowRange;

/**
 * @deprecated 当sdk因为其他需求有主体jar要求的时候这边可以删掉
 * 主体代码里面的这个类被混淆了，sdk抄一份一样的
 * @author Carl
 * 填报的时候处理跨域的值
 */

public class SheetInterval4CheckNameSpace extends AbstractNameSpace {
	public static final SheetInterval4CheckNameSpace SC = new SheetInterval4CheckNameSpace();
	
	private SheetInterval4CheckNameSpace(){}
	
	public static SheetInterval4CheckNameSpace getInstance() {
		return SC;
	}

	@Override
	public Object getVariable(Object var, CalculatorProvider calculator) {
		// TODO carl：真是乱
		if (!(var instanceof SheetIntervalLiteral)) {
			return null;
		}
		
		Report report = calculator.getAttribute(Report.KEY);
		if (report == null) {
			return null;
		}

		FineBook wb = report.getBook();
		SheetIntervalLiteral sil = (SheetIntervalLiteral)var;
		if (wb == null || StringUtils.isBlank(sil.getSheetName())) {
			return null;
		}

		for (int i = 0; i < wb.getReportCount(); i++) {
			if (ComparatorUtils.equals(wb.getReportName(i), sil.getSheetName())) {
				Report rp = wb.getReport(i);
				if (rp instanceof WriteECReport) {
					if (sil.getSheetAtom() instanceof ColumnRowRange) {
						return ((WriteECReport)rp).resolveColumnRowRange((ColumnRowRange)sil.getSheetAtom(), null);
					} else {
						// TODO 暂时只有 ColumnRowRange和Ambiguity两种，考虑到以后会加就弄到外面来处理
						// Ambiguity的情况不处理
						FineLoggerFactory.getLogger().error("UnResolvedVariable " + sil.toString());
					}
				}
				break;
			}
		}

		return null;
	}

}