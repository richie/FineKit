package com.fanruan.api.cal;

import com.fr.general.FArray;
import com.fr.stable.ArrayProvider;

import java.util.Collection;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-29
 * 创建容器的工具类
 */
public class ContainerKit {

    /**
     * 创建一个插件可识别的数组对象
     *
     * @param array 原始数组
     * @param <T>   数组元素类型
     * @return 插件可识别的数组对象
     */
    public static <T> ArrayProvider<T> newArray(T[] array) {
        return new FArray<>(array);
    }

    /**
     * 创建一个插件可识别的数组对象
     *
     * @param collection 原始集合
     * @param <T>        数组元素类型
     * @return 插件可识别的数组对象
     */
    public static <T> ArrayProvider<T> newArray(Collection<T> collection) {
        return new FArray<>(collection);
    }
}
