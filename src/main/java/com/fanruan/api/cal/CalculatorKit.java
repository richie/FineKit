package com.fanruan.api.cal;

import com.fanruan.api.cal.namespace.SheetInterval4CheckNameSpace;
import com.fanruan.api.session.SessionKit;
import com.fr.base.ParameterMapNameSpace;
import com.fr.base.TableDataNameSpace;
import com.fr.data.TableDataSource;
import com.fr.report.core.namespace.SimpleCellValueNameSpace;
import com.fr.report.report.Report;
import com.fr.script.Calculator;
import com.fr.stable.StringUtils;
import com.fr.stable.script.CalculatorProvider;
import com.fr.stable.script.NameSpace;
import com.fr.stable.web.SessionProvider;
import com.fr.web.core.ReportSessionIDInfor;
import com.fr.web.core.TemplateSessionIDInfo;
import com.fr.web.session.SessionIDInfo;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * @author zack
 * @version 10.0
 * 帆软算子工具类（主要用于公式计算）
 * @date 2019/8/23
 */
public class CalculatorKit {
    /**
     * 创建一个基础算子（基础算子仅支持函数计算.比如SUM()函数）
     *
     * @return 算子对象
     */
    public static CalculatorProvider createCalculator() {
        return Calculator.createCalculator();
    }

    /**
     * 创建一个带参数计算的算子
     * （可以支持计算入参paraMap里面相关的带参函数，
     * 比如paraMap带有参数a->1,b->2,CalculatorKit.createCalculator(paraMap).evalValue("SUM（a，b）")输出3）
     *
     * @return 算子
     */
    public static CalculatorProvider createCalculator(Map paraMap) {
        ParameterMapNameSpace nameSpace = ParameterMapNameSpace.create(paraMap);
        CalculatorProvider calculator = createCalculator();
        calculator.pushNameSpace(nameSpace);
        return calculator;
    }

    /**
     * 创建一个支持单元格以及模板数据集计算的算子
     *
     * @param sessionID 模板的sessionID
     * @param paraMap   其他参数
     * @return 算子对象
     */
    public static CalculatorProvider createCalculator(@Nullable String sessionID, @Nullable Map paraMap) {
        ParameterMapNameSpace nameSpace = ParameterMapNameSpace.create(paraMap);
        CalculatorProvider calculator = createCalculator();
        calculator.pushNameSpace(nameSpace);
        if (StringUtils.isNotEmpty(sessionID)) {
            SessionProvider rsii = SessionKit.getSession(sessionID);
            if (rsii != null) {
                if (rsii instanceof TemplateSessionIDInfo) {
                    //支持模板单元格值计算
                    calculator.setAttribute(TableDataSource.KEY, ((TemplateSessionIDInfo) rsii).getTableDataSource());
                    calculator.pushNameSpace(SessionIDInfo.asNameSpace(sessionID));
                    if (rsii instanceof ReportSessionIDInfor) {
                        //支持跨sheet计算
                        calculator.pushNameSpace(SheetInterval4CheckNameSpace.getInstance());
                        calculator.setAttribute(Report.KEY, ((ReportSessionIDInfor) rsii).getReport2Show(0));
                    }
                }
            }
        }
        return calculator;
    }

    /**
     * 返回服务器数据集的算子空间（可以通过调用calculator.pushNameSpace()将算子空间塞进算子实例，从而支持服务器数据集相关的函数计算）
     *
     * @return 服务器数据集名字空间
     */
    public static NameSpace getServerTableDataNameSpace() {
        return TableDataNameSpace.getInstance();
    }

    /**
     * 用于计算单元格的名字空间
     *
     * @return 单元格名字空间
     */
    public static NameSpace createSimpleCellValueNameSpace() {
        return SimpleCellValueNameSpace.getInstance();
    }
}