package com.fanruan.api.cal;

import com.fr.base.Parameter;
import com.fr.base.ParameterHelper;
import com.fr.base.ParameterMapNameSpace;
import com.fr.stable.ParameterProvider;
import com.fr.stable.script.NameSpace;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 * 参数相关的工具类
 */
public class ParameterKit {

    /**
     * 创建一个参数对象实例
     *
     * @return 参数对象
     */
    public static ParameterProvider newParameter() {
        return new Parameter();
    }

    /**
     * 创建一个参数对象实例
     *
     * @param name  参数名
     * @param value 参数值
     * @return 参数对象
     */
    public static ParameterProvider newParameter(String name, Object value) {
        return new Parameter(name, value);
    }

    /**
     * 从字符串中分析中有哪些需要的参数
     *
     * @param text         字符串
     * @param hasColumnRow 是否需要分析格子类型的参数
     * @return 字符串中包含的参数集合
     */
    public static Parameter[] analyze4Parameters(String text, boolean hasColumnRow) {
        return ParameterHelper.analyze4Parameters(text, hasColumnRow);
    }

    /**
     * 分析公式中所携带的参数
     *
     * @param text 公式内容
     * @return 参数数组
     */
    public static @NotNull ParameterProvider[] analyze4ParametersFromFormula(String text) {
        return ParameterHelper.analyze4ParametersFromFormula(text);
    }

    /**
     * 分析一组字符串中的参数
     *
     * @param paramTexts 字符串组
     * @param isFormula  是否是公式类型的字符串数组
     * @return 参数集合
     */
    public static @NotNull ParameterProvider[] analyze4Parameters(String[] paramTexts, boolean isFormula) {
        return ParameterHelper.analyze4Parameters(paramTexts, isFormula);
    }

    /**
     * 创建一个用于计算的参数对名字空间
     *
     * @param map 参数键值对
     * @return 名字空间，用于传递给算子做计算
     */
    public static @NotNull NameSpace createParameterMapNameSpace(Map<String, Object> map) {
        return ParameterMapNameSpace.create(map);
    }

    /**
     * 创建一个用于计算的参数对名字空间
     *
     * @param ps 参数数组数组
     * @return 名字空间，用于传递给算子做计算
     */

    public static @NotNull ParameterMapNameSpace createParameterMapNameSpace(ParameterProvider[] ps) {
        return ParameterMapNameSpace.create(ps);
    }
}
