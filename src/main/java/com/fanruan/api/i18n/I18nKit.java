package com.fanruan.api.i18n;
import com.fr.locale.InterProviderFactory;

public class I18nKit {
    /**
     * 获取国际化文本
     *
     * @param string 需要国际化的值对应的键
     * @return 返回国际化文本
     */
    public static String getLocText(String string) {
        return InterProviderFactory.getProvider().getLocText(string);
    }
}
