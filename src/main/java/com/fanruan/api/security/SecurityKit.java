package com.fanruan.api.security;

import com.fr.security.SecurityToolbox;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 * 文本加密解密相关的工具类
 */
public class SecurityKit {
    /**
     * RSA加密
     * @param plainText 要加密的文本内容
     * @return 加密后的内容
     */
    public static String encrypt(String plainText) {
        return SecurityToolbox.encrypt(plainText);
    }
    /**
     * RSA解密
     * @param cipherText 密文数据
     * @return 解密后的内容
     */
    public static String decrypt(String cipherText) {
        return SecurityToolbox.decrypt(cipherText);
    }
}
