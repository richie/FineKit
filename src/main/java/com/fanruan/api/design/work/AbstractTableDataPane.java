package com.fanruan.api.design.work;

import com.fr.base.TableData;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 * 数据插件继承此抽象类
 */
public abstract class AbstractTableDataPane<T extends TableData>  extends com.fr.design.data.tabledata.tabledatapane.AbstractTableDataPane<T> {
}
