package com.fanruan.api.design.work.formula;

import com.fr.design.formula.UIFormula;

/**
 * 获取公式编辑器面板工具类
 */
public class FormulaUIKit {

    /**
     * 获取普通公式面板
     *
     * @return 公式面板
     */
    public static UIFormula createFormulaPane() {
        return com.fr.design.formula.FormulaFactory.createFormulaPane();
    }

    /**
     * 获取可设置导出excel时是否保留公式的公式面板
     *
     * @return 公式面板
     */
    public static UIFormula createFormulaPaneWhenReserveFormula() {
        return com.fr.design.formula.FormulaFactory.createFormulaPaneWhenReserveFormula();
    }
}
