package com.fanruan.api.design.work;


import com.fanruan.api.design.ui.component.UIButton;
import com.fanruan.api.design.ui.component.UIComboBox;
import com.fanruan.api.design.ui.component.UILabel;
import com.fanruan.api.design.ui.container.BasicBeanPane;
import com.fanruan.api.log.LogKit;
import com.fanruan.api.macro.EncodeConstants;
import com.fanruan.api.util.ArrayKit;
import com.fanruan.api.util.StringKit;
import com.fr.data.impl.Connection;
import com.fr.data.impl.JDBCDatabaseConnection;
import com.fr.data.impl.JNDIDatabaseConnection;
import com.fr.data.operator.DataOperator;
import com.fr.design.data.datapane.connect.JDBCDefPane;
import com.fr.design.data.datapane.connect.JNDIDefPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.scrollruler.ModLineBorder;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * 数据连接面板
 *
 * @param <E> 数据连接类型
 */
public abstract class DatabaseConnectionPane<E extends com.fr.data.impl.Connection> extends BasicBeanPane<Connection> {

    private UILabel message;
    private UIButton okButton;
    private UIButton cancelButton;
    private JDialog dialog;
    private UILabel uiLabel;

    private UIComboBox<String> charSetComboBox;
    private String originalCharSet = null;

    public DatabaseConnectionPane() {
        this.initComponents();
    }

    protected void initComponents() {
        message = new UILabel();
        uiLabel = new UILabel();
        okButton = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_OK"));
        cancelButton = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Cancel"));
        String[] defaultEncode = new String[]{com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Encode_Auto")};
        charSetComboBox = new UIComboBox<>(ArrayKit.addAll(defaultEncode, EncodeConstants.ENCODING_ARRAY));
        this.setLayout(new BorderLayout());
        JPanel northPane = FRGUIPaneFactory.createY_AXISBoxInnerContainer_L_Pane();
        this.add(northPane, BorderLayout.NORTH);

        JPanel testPane = FRGUIPaneFactory.createNormalFlowInnerContainer_M_Pane();
        northPane.add(testPane, BorderLayout.NORTH);
        UIButton testButton = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Datasource_Test_Connection"));
        testPane.add(testButton);
        testButton.addActionListener(testConnectionActionListener);
        testPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 4));

        northPane.add(mainPanel(), BorderLayout.CENTER);

        JPanel chartSetPane = FRGUIPaneFactory.createNColumnGridInnerContainer_S_Pane(2);
        northPane.add(chartSetPane);
        chartSetPane.setBorder(BorderFactory.createTitledBorder(
                new ModLineBorder(ModLineBorder.TOP),
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Advanced")
        ));
        chartSetPane.add(GUICoreUtils.createNamedPane(charSetComboBox, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Datasource_Charset") + ":"));
    }

    protected abstract JPanel mainPanel();

    @Override
    public void populateBean(com.fr.data.impl.Connection ob) {
        this.originalCharSet = ob.getOriginalCharsetName();
        if (StringKit.isBlank(originalCharSet)) {
            this.charSetComboBox.setSelectedItem(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Encode_Auto"));
        } else {
            this.charSetComboBox.setSelectedItem(ob.getOriginalCharsetName());
        }

        populateSubDatabaseConnectionBean((E) ob);
    }

    protected abstract void populateSubDatabaseConnectionBean(E ob);

    @Override
    public com.fr.data.impl.Connection updateBean() {
        E ob = updateSubDatabaseConnectionBean();

        ob.setOriginalCharsetName(this.originalCharSet);
        if (this.charSetComboBox.getSelectedIndex() == 0) {
            ob.setNewCharsetName(null);
            ob.setOriginalCharsetName(null);
        } else {
            ob.setNewCharsetName(EncodeConstants.ENCODING_GBK);
            ob.setOriginalCharsetName(((String) this.charSetComboBox.getSelectedItem()));

        }

        return ob;
    }

    protected abstract E updateSubDatabaseConnectionBean();

    ActionListener testConnectionActionListener = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {

            final SwingWorker connectionThread = new SwingWorker() {
                protected Object doInBackground() throws Exception {
                    try {
                        com.fr.data.impl.Connection database = DatabaseConnectionPane.this.updateBean();
                        boolean connect = DataOperator.getInstance().testConnection(database);
                        okButton.setEnabled(true);
                        message.setText(database.connectMessage(connect));
                        if (connect) {
                            uiLabel.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
                            message.setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Datasource_Connection_Successfully"));
                        } else {
                            uiLabel.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
                            message.setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Connection_Failed"));
                        }
                    } catch (Exception exp) {
                        LogKit.error(exp.getMessage(), exp);
                    }
                    return null;
                }
            };

            connectionThread.execute();
            initDialogPane();
            okButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.dispose();
                }
            });
            cancelButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.dispose();
                    connectionThread.cancel(true);
                }
            });

            dialog.addWindowListener(new WindowAdapter() {
                public void windowClosed(WindowEvent e) {
                    connectionThread.cancel(true);
                }
            });

            dialog.show();
            dialog.dispose();
        }
    };

    private void initDialogPane() {

        message.setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Datasource_Test_Connection") + "...");
        message.setBorder(BorderFactory.createEmptyBorder(8, 5, 0, 0));
        okButton.setEnabled(false);

        dialog = new JDialog((Dialog) SwingUtilities.getWindowAncestor(DatabaseConnectionPane.this), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Datasource_Test_Connection"), true);
        dialog.setSize(new Dimension(268, 118));
        okButton.setEnabled(false);
        JPanel jp = new JPanel();
        JPanel upPane = new JPanel();
        JPanel downPane = new JPanel();
        uiLabel = new UILabel(UIManager.getIcon("OptionPane.informationIcon"));

        upPane.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        upPane.add(uiLabel);
        upPane.add(message);
        downPane.setLayout(new FlowLayout(FlowLayout.CENTER, 6, 0));
        downPane.add(okButton);
        downPane.add(cancelButton);
        jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
        jp.add(upPane);
        jp.add(downPane);
        dialog.add(jp);
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(SwingUtilities.getWindowAncestor(DatabaseConnectionPane.this));
    }


    public static class JDBC extends com.fr.design.data.datapane.connect.DatabaseConnectionPane<JDBCDatabaseConnection> {
        private static JDBCDefPane jdbcDefPane = new JDBCDefPane();

        @Override
        protected JPanel mainPanel() {
            return jdbcDefPane;
        }

        @Override
        protected boolean isFineBI() {
            return false;
        }

        @Override
        protected void populateSubDatabaseConnectionBean(JDBCDatabaseConnection ob) {
            jdbcDefPane.populate(ob);
        }

        @Override
        protected JDBCDatabaseConnection updateSubDatabaseConnectionBean() {
            return jdbcDefPane.update();
        }

        @Override
        protected String title4PopupWindow() {
            return "JDBC";
        }
    }

    public static class JNDI extends com.fr.design.data.datapane.connect.DatabaseConnectionPane<JNDIDatabaseConnection> {
        private static JNDIDefPane jndiDefPane = new JNDIDefPane();

        @Override
        protected JPanel mainPanel() {
            return jndiDefPane;
        }

        @Override
        protected boolean isFineBI() {
            return false;
        }

        @Override
        protected void populateSubDatabaseConnectionBean(JNDIDatabaseConnection ob) {
            jndiDefPane.populate(ob);
        }

        @Override
        protected JNDIDatabaseConnection updateSubDatabaseConnectionBean() {
            return jndiDefPane.update();
        }

        @Override
        protected String title4PopupWindow() {
            return "JNDI";
        }
    }

}