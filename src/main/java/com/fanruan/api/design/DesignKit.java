package com.fanruan.api.design;

import com.fr.base.TableData;
import com.fr.design.data.datapane.preview.PreviewTablePane;
import com.fr.design.i18n.Toolkit;

public class DesignKit {
    /**
     * 直接预览数据集，没有实际值及显示值
     * @param tableData 数据集
     */
    public static void previewTableData(TableData tableData){
        PreviewTablePane.previewTableData(tableData, -1, -1);
    }

    /**
     * 文本国际化
     * @param key 国际化键
     * @return 国际化后的值
     */
    public static String i18nText(String key) {
        return Toolkit.i18nText(key);
    }

    /**
     * 带参数的文本国际化
     * @param key 国际化键
     * @param args 参数
     * @return 国际化后的值
     */
    public static String i18nText(String key, Object... args) {
        return Toolkit.i18nText(key, args);
    }
}
