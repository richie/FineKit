package com.fanruan.api.design.ui.component.code;

import com.fr.design.gui.syntax.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 代码编辑器，支持javascript、sql、java、公式和普通文本
 * @see com.fanruan.api.design.ui.component.code.SyntaxConstants
 * <pre> {@code
 *
 *   UISyntaxTextArea contentTextArea = new UISyntaxTextArea();
 *   contentTextArea.setCloseCurlyBraces(true);
 *   contentTextArea.setLineWrap(true);
 *
 *   contentTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
 *   contentTextArea.setCodeFoldingEnabled(true);
 *   contentTextArea.setAntiAliasingEnabled(true);
 * }
 * </pre>
 */
public class UISyntaxTextArea extends RSyntaxTextArea {

    public UISyntaxTextArea() {
        super();
    }

    public UISyntaxTextArea(String text) {
        super(text);
    }

    public UISyntaxTextArea(int rows, int cols) {
        super(rows, cols);
    }

    public UISyntaxTextArea(String text, int rows, int cols) {
        super(text, rows, cols);
    }
}
