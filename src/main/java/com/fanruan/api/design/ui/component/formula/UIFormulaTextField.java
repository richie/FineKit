package com.fanruan.api.design.ui.component.formula;

import com.fr.design.formula.TinyFormulaPane;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 带公式选择器的公式输入文本框
 */
public class UIFormulaTextField extends TinyFormulaPane {

    public UIFormulaTextField() {
        super();
    }

    public UIFormulaTextField(String text) {
        super();
        setText(text);
    }

    /**
     * 设置公式文本框的内容
     * @param text 公式文本
     */
    public void setText(String text) {
        populateBean(text);
    }

    /**
     * 获取公式文本框的内容
     * @return 公式内容
     */
    public String getText() {
        return updateBean();
    }

    /**
     * 公式窗口点击确定后的事件接口，该方法一般用于重载
     */
    public void okEvent() {

    }

}
