package com.fanruan.api.design.ui.layout;

import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;

import javax.swing.*;
import java.awt.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 表格布局
 * <pre> {@code
 *   double p = TableLayoutKit.PREFERRED;
 *   double f = TableLayoutKit.FILL;
 *   double[] rowSize = new double[]{p, p, p, f};
 *   double[] columnSize = new double[]{p, f};
 *   JComponent[][] comps = new JComponent[][]{
 *        {new JLabel(), new JButton()},
 *        {new JLabel(), new JButton()},
 *        {new JLabel(), new JButton()}
 *   };
 *   JPanel = TableLayoutKit.createTableLayoutPane(comps, rowSize, columnSize);
 * }
 * </pre>
 */
public class TableLayoutKit {

    /**
     * 按需计算尺寸
     */
    public static final double PREFERRED = TableLayout.PREFERRED;

    /**
     * 按充满计算尺寸
     */
    public static final double FILL = TableLayout.FILL;

    /**
     * 创建一个简单的表格布局并把组件添加到布局容器中
     *
     * @param components 组件
     * @param rowSize    行尺寸信息，可以是固定的数值，也可以是PREFERRED或者FILL常量
     * @param columnSize 列尺寸信息，可以是固定的数值，也可以是PREFERRED或者FILL常量
     * @return 按要求的布局添加好组件的容器
     */
    public static JPanel createTableLayoutPane(Component[][] components, double[] rowSize, double[] columnSize) {
        return TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
    }

    /**
     * 创建一个简单的表格布局并把组件添加到布局容器中
     *
     * @param components 组件
     * @param rowSize    行尺寸信息，可以是固定的数值，也可以是PREFERRED或者FILL常量
     * @param columnSize 列尺寸信息，可以是固定的数值，也可以是PREFERRED或者FILL常量
     * @param gap        垂直和水平间隙
     * @return 按要求的布局添加好组件的容器
     */
    public static JPanel createCommonTableLayoutPane(Component[][] components, double[] rowSize, double[] columnSize, double gap) {

        return TableLayoutHelper.createCommonTableLayoutPane(components, rowSize, columnSize, gap);
    }

    /**
     * 创建一个简单的表格布局并把组件添加到布局容器中
     *
     * @param components    组件
     * @param rowSize       行尺寸信息，可以是固定的数值，也可以是PREFERRED或者FILL常量
     * @param columnSize    列尺寸信息，可以是固定的数值，也可以是PREFERRED或者FILL常量
     * @param horizontalGap 水平间隙
     * @param verticalGap   垂直间隙
     * @return 按要求的布局添加好组件的容器
     */
    public static JPanel createGapTableLayoutPane(Component[][] components, double[] rowSize, double[] columnSize, double horizontalGap, double verticalGap) {

        return TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, horizontalGap, verticalGap);
    }
}
