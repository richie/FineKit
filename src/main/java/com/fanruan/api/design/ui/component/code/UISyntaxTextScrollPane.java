package com.fanruan.api.design.ui.component.code;

import com.fr.design.gui.syntax.ui.rtextarea.RTextScrollPane;

import java.awt.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 可滚动的代码编辑器容器
 */
public class UISyntaxTextScrollPane extends RTextScrollPane {

    public UISyntaxTextScrollPane() {

    }

    public UISyntaxTextScrollPane(Component component) {
        super(component);
    }

    public UISyntaxTextScrollPane(Component component, boolean lineNumbers) {
        super(component, lineNumbers);
    }

    public UISyntaxTextScrollPane(Component component, boolean lineNumbers, Color lineNumberColor) {
        super(component, lineNumbers, lineNumberColor);
    }
}
