package com.fanruan.api.design.ui.component;

import javax.swing.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 标签组件
 */
public class UILabel extends com.fr.design.gui.ilable.UILabel {

    public UILabel() {

    }

    public UILabel(String text) {
        super(text);
    }

    public UILabel(Icon icon) {
        super(icon);
    }

    public UILabel(String text, boolean enable) {
        super(text, enable);
    }

    public UILabel(String text, int align) {
        super(text, align);
    }

    public UILabel(Icon icon, int align) {
        super(icon, align);
    }

    public UILabel(String text, Icon icon, int align) {
        super(text, icon, align);
    }

}
