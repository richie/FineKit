package com.fanruan.api.design.ui.component;

import javax.swing.*;

/**
 * @author zhaojunzhe
 * @version 10.0
 * Created by zhaojunzhe on 2019-08-28
 * 复选框组件
 */
public class UICheckBox extends com.fr.design.gui.icheckbox.UICheckBox {

    public UICheckBox() {
    }

    public UICheckBox(String text) {
        super(text);
    }


    public UICheckBox(String text, boolean checked) {
        super(text, checked);
    }

    public UICheckBox(String text, Icon icon) {
        super(text, icon);
    }
}
