package com.fanruan.api.design.ui.container;


import com.fanruan.api.design.util.GUICoreKit;
import com.fr.design.dialog.DialogActionListener;
import com.fr.design.dialog.UIDialog;

import java.awt.*;

public abstract class BasicPane extends com.fr.design.dialog.BasicPane {

    /**
     * 显示窗口
     *
     * @param window 窗口
     * @return 对话框
     */
    public BasicDialog showWindow(Window window) {
        return this.showWindow(window, null);
    }


    /**
     * 显示窗口
     *
     * @param window 窗口
     * @param l      对话框监听器
     * @return 对话框
     */
    public BasicDialog showWindow(Window window, DialogActionListener l) {
        return showWindowWithCustomSize(window, l, BasicDialog.DEFAULT);
    }

    public BasicDialog showWindowWithCustomSize(Window window, DialogActionListener l, Dimension dimension) {
        BasicDialog dg;
        if (window instanceof Frame) {
            dg = new DIALOG((Frame) window);
        } else {
            dg = new DIALOG((Dialog) window);
        }

        if (l != null) {
            dg.addDialogActionListener(l);
        }
        dg.setBasicDialogSize(dimension);
        GUICoreKit.centerWindow(dg);
        dg.setResizable(false);
        return dg;
    }

    private class DIALOG extends BasicDialog {
        public DIALOG(Frame parent) {
            super(parent, BasicPane.this);
            this.setTitle(BasicPane.this.title4PopupWindow());
        }

        public DIALOG(Dialog parent) {
            super(parent, BasicPane.this);
            this.setTitle(BasicPane.this.title4PopupWindow());
        }


        public DIALOG(Frame parent, boolean isNeedButtonPane) {
            super(parent, BasicPane.this, isNeedButtonPane);
            this.setTitle(BasicPane.this.title4PopupWindow());
        }


        public DIALOG(Dialog parent, boolean isNeedButtonPane) {
            super(parent, BasicPane.this, isNeedButtonPane);
            this.setTitle(BasicPane.this.title4PopupWindow());
        }


        public void checkValid() throws Exception {
            BasicPane.this.checkValid();
        }

    }

    private class UnsizedDialog extends UIDialog {

        public UnsizedDialog(Frame parent) {
            super(parent, BasicPane.this);
            this.setTitle(BasicPane.this.title4PopupWindow());
        }

        public UnsizedDialog(Dialog parent) {
            super(parent, BasicPane.this);
            this.setTitle(BasicPane.this.title4PopupWindow());
        }


        public void checkValid() throws Exception {
            BasicPane.this.checkValid();
        }
    }
}
