package com.fanruan.api.design.ui.component;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 密码输入控件
 */
public class UIPasswordField extends com.fr.design.gui.ipasswordfield.UIPassWordField {

    public UIPasswordField() {
        super();
    }

    public UIPasswordField(String text) {
        super(text);
    }

}
