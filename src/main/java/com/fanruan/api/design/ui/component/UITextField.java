package com.fanruan.api.design.ui.component;

import javax.swing.text.Document;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 文本框组件
 */
public class UITextField  extends com.fr.design.gui.itextfield.UITextField {

    public UITextField() {

    }

    public UITextField(int columns) {
        super(columns);
    }

    public UITextField(String text) {
        super(text);
    }

    public UITextField(String text, int columns) {
        super(text, columns);
    }

    public UITextField(Document document, String text, int columns) {
        super(document, text, columns);
    }
}
