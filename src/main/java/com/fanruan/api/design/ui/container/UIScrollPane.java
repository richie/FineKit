package com.fanruan.api.design.ui.container;

import java.awt.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 可滚动容器
 */
public class UIScrollPane extends com.fr.design.gui.icontainer.UIScrollPane {

    public UIScrollPane(Component component) {
        super(component);
    }
}
