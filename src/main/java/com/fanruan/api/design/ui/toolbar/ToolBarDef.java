package com.fanruan.api.design.ui.toolbar;

import com.fanruan.api.design.ui.component.UIToolbar;
import com.fr.design.gui.itoolbar.UIToolBarUI;

import javax.swing.*;
import java.awt.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 */
public class ToolBarDef extends com.fr.design.menu.ToolBarDef {

    public static UIToolbar createJToolBar(final Color background) {
        UIToolbar toolbar = new UIToolbar(FlowLayout.LEFT);
        toolbar.setUI(new UIToolBarUI() {
            @Override
            public void paint(Graphics g, JComponent c) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setColor(background);
                g2.fillRect(0, 0, c.getWidth(), c.getHeight());
            }
        });
        toolbar.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
        return toolbar;
    }

    public static UIToolbar createJToolBar() {
        UIToolbar toolbar = new UIToolbar(FlowLayout.LEFT);
        toolbar.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
        return toolbar;
    }

}
