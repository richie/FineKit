package com.fanruan.api.design.ui.component;

import com.fr.design.gui.itextfield.PlaceholderTextField;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 带占位文本的文本编辑器（当没有输入文本的时候，会显示占位符）
 */
public class UIPlaceholderTextField extends PlaceholderTextField {

    public UIPlaceholderTextField() {
        super();
    }

    public UIPlaceholderTextField(int columns) {
        super(columns);
    }

    public UIPlaceholderTextField(String placeHolder) {
        super();
        setPlaceholder(placeHolder);
    }

    public UIPlaceholderTextField(String text, String placeHolder) {
        super(text);
        setPlaceholder(placeHolder);
    }

}
