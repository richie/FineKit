package com.fanruan.api.design.ui.component.table.model;


import com.fanruan.api.cal.ParameterKit;
import com.fanruan.api.design.DesignKit;
import com.fanruan.api.design.ui.component.UILabel;
import com.fanruan.api.design.ui.component.UITextField;
import com.fanruan.api.design.ui.component.table.action.UITableEditAction;
import com.fanruan.api.design.ui.editor.ValueEditorPane;
import com.fanruan.api.design.ui.editor.ValueEditors;
import com.fanruan.api.log.LogKit;
import com.fanruan.api.util.IOKit;
import com.fanruan.api.util.StringKit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.stable.ParameterProvider;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Collections;

/**
 * 编辑参数的表格模型，通常来说就是两列：参数名和参数值
 */
public class ParameterTableModel extends UITableModelAdapter<ParameterProvider> {

    public static final int NO_CHART_USE = 0;
    public static final int CHART_NORMAL_USE = 1;
    public static final int FORM_NORMAL_USE = -1;

    private static final long serialVersionUID = 1L;
    protected Component component = null;

    public ParameterTableModel() {
        this(NO_CHART_USE);
    }

    public ParameterTableModel(int paraUseType) {
        super(new String[]{DesignKit.i18nText("Fine-Design_Basic_Parameter"), DesignKit.i18nText("Fine-Design_Basic_Value")});
        this.setColumnClass(new Class[]{ParameterEditor.class, ParameterValueEditor.class});
        this.setDefaultEditor(ParameterValueEditor.class, new ParameterValueEditor(paraUseType));
        this.setDefaultEditor(ParameterEditor.class, new ParameterEditor());
        this.setDefaultRenderer(ParameterValueEditor.class, new ParameterValueRenderer(paraUseType));
    }

    public ParameterTableModel(int paraUseType, Component component) {
        super(new String[]{DesignKit.i18nText("Fine-Design_Basic_Parameter"), DesignKit.i18nText("Fine-Design_Basic_Value")});
        this.setColumnClass(new Class[]{ParameterEditor.class, ParameterValueEditor.class});
        this.setDefaultEditor(ParameterValueEditor.class, new ParameterValueEditor(paraUseType));
        this.setDefaultEditor(ParameterEditor.class, new ParameterEditor());
        this.setDefaultRenderer(ParameterValueEditor.class, new ParameterValueRenderer(paraUseType));
        this.component = component;
    }

    public ParameterTableModel(ValueEditorPane valueEditorPane, ValueEditorPane valueRenderPane, Component component) {
        super(new String[]{DesignKit.i18nText("Fine-Design_Basic_Parameter"), DesignKit.i18nText("Fine-Design_Basic_Value")});
        this.setColumnClass(new Class[]{ParameterEditor.class, ParameterValueEditor.class});
        this.setDefaultEditor(ParameterValueEditor.class, new ParameterValueEditor(valueEditorPane));
        this.setDefaultEditor(ParameterEditor.class, new ParameterEditor());
        this.setDefaultRenderer(ParameterValueEditor.class, new ParameterValueRenderer(valueRenderPane));
        this.component = component;
    }

    /**
     * 单元格是否可编辑
     *
     * @param row 行
     * @param col 列
     * @return 是否可以编辑
     */
    public boolean isCellEditable(int row, int col) {
        if (col == 1) {
            return this.getList().get(row) != null && StringKit.isNotEmpty(this.getList().get(row).getName());
        }
        return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ParameterProvider para = this.getList().get(rowIndex);
        switch (columnIndex) {
            case 0:
                return para.getName();
            case 1:
                return para.getValue();
        }
        return null;
    }

    /**
     * 生成工具栏上的一系列动作按钮
     *
     * @return 返回table上的action数组.
     */
    @Override
    public UITableEditAction[] createAction() {
        return new UITableEditAction[]{new AddParameterAction(), new DeleteAction(), new MoveUpAction(), new MoveDownAction()};
    }

    protected class AddParameterAction extends AddTableRowAction {

        public AddParameterAction() {
            super();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            addParameter();
        }
    }

    private void addParameter() {
        ParameterProvider para = ParameterKit.newParameter();
        addRow(para);
        fireTableDataChanged();
        table.getSelectionModel().setSelectionInterval(table.getRowCount() - 1, table.getRowCount() - 1);
    }

    protected class DeleteAction extends UITableEditAction {

        private Component component = null;

        public DeleteAction() {
            this.setName(DesignKit.i18nText("Fine-Design_Report_Delete"));
            this.setSmallIcon(IOKit.readIcon("/com/fr/base/images/cell/control/remove.png"));
        }

        public DeleteAction(Component component) {
            this.setName(DesignKit.i18nText("Fine-Design_Report_Delete"));
            this.setSmallIcon(IOKit.readIcon("/com/fr/base/images/cell/control/remove.png"));
            this.component = component;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int[] selectedRow = table.getSelectedRows();
            if (isMultiSelected()) {
                JOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), DesignKit.i18nText("Fine-Design_Basic_Multiple_Select_Warn_Text"));
                return;
            }
            if (table.getCellEditor() != null) {
                try {
                    table.getCellEditor().stopCellEditing();
                } catch (Exception ee) {
                    LogKit.error(ee.getMessage(), ee);
                }
            }
            if (getRowCount() < 1) {
                return;
            }

            if (component == null) {
                component = DesignerContext.getDesignerFrame();
            }
            int val = JOptionPane.showConfirmDialog(component,
                    DesignKit.i18nText("Fine-Design_Basic_Utils_Are_You_Sure_To_Remove_The_Selected_Item") + "?", DesignKit.i18nText("Fine-Design_Basic_Remove"),
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (val != JOptionPane.OK_OPTION) {
                return;
            }
            for (int i = 0; i < selectedRow.length; i++) {
                if (selectedRow[i] - i < 0) {
                    continue;
                }
                removeRow(selectedRow[i] - i);
            }
            fireTableDataChanged();
            int selection = selectedRow[0] > table.getRowCount() ? table.getRowCount() - 1
                    : (selectedRow[0] > 1 ? selectedRow[0] - 1 : 0);
            table.getSelectionModel().setSelectionInterval(selection, selection);
        }

        private boolean isMultiSelected() {
            int[] selectedRow = table.getSelectedRows();
            return (selectedRow.length == 1 && (selectedRow[0] > table.getRowCount() - 1 || selectedRow[0] < 0)) || selectedRow.length == 0;
        }

        @Override
        public void checkEnabled() {
            setEnabled(!isMultiSelected());
        }
    }

    protected class MoveUpAction extends UITableEditAction {

        public MoveUpAction() {
            this.setName(DesignKit.i18nText("Fine-Design_Basic_Utils_Move_Up"));
            this.setSmallIcon(IOKit.readIcon("/com/fr/design/images/control/up.png"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedRow = table.getSelectedRow();
            stopCellEditing();
            if (getList().size() < 2 || selectedRow == 0) {
                return;
            }
            Collections.swap(getList(), selectedRow, selectedRow - 1);
            fireTableDataChanged();
            table.getSelectionModel().setSelectionInterval(selectedRow - 1, selectedRow - 1);
        }

        @Override
        public void checkEnabled() {
        }
    }

    protected class MoveDownAction extends UITableEditAction {

        public MoveDownAction() {
            this.setName(DesignKit.i18nText("Fine-Design_Basic_Utils_Move_Down"));
            this.setSmallIcon(IOKit.readIcon("/com/fr/design/images/control/down.png"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedRow = table.getSelectedRow();
            stopCellEditing();
            if (getList().size() < 2 || selectedRow == getRowCount() - 1) {
                return;
            }
            Collections.swap(getList(), selectedRow, selectedRow + 1);
            fireTableDataChanged();
            table.getSelectionModel().setSelectionInterval(selectedRow + 1, selectedRow + 1);
        }

        @Override
        public void checkEnabled() {
        }
    }

    public class ParameterEditor extends AbstractCellEditor implements TableCellEditor {

        private UITextField textField;

        public ParameterEditor() {
            textField = new UITextField();
            this.addCellEditorListener(new CellEditorListener() {

                @Override
                public void editingCanceled(ChangeEvent e) {

                }

                @Override
                public void editingStopped(ChangeEvent e) {
                    if (table.getSelectedRow() == -1) {
                        return;
                    }
                    ParameterProvider para = getList().get(table.getSelectedRow());
                    String value = StringKit.trimToNull(textField.getText());
                    para.setName(value);
                    fireTableDataChanged();
                }
            });
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            textField.setText((String) value);
            return textField;
        }

        @Override
        public Object getCellEditorValue() {
            return textField.getText();
        }
    }

    private class ParameterValueEditor extends AbstractCellEditor implements TableCellEditor {
        private static final long serialVersionUID = 1L;
        private ValueEditorPane editor;

        public ParameterValueEditor(int paraUseType) {
            this(ValueEditors.createValueEditorPaneWithUseType(paraUseType));
        }

        public ParameterValueEditor(ValueEditorPane valueEditorPane) {

            editor = valueEditorPane;

            this.addCellEditorListener(new CellEditorListener() {

                @Override
                public void editingCanceled(ChangeEvent e) {

                }

                @Override
                public void editingStopped(ChangeEvent e) {
                    if (table.getSelectedRow() == -1) {
                        return;
                    }
                    ParameterProvider para = getList().get(table.getSelectedRow());
                    para.setValue(getCellEditorValue());
                    fireTableDataChanged();
                }
            });
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            editor.populate(value == null ? StringKit.EMPTY : value);
            return editor;
        }

        @Override
        public Object getCellEditorValue() {
            return editor.update();
        }

    }

    private class ParameterValueRenderer extends DefaultTableCellRenderer {
        private static final long serialVersionUID = 1L;
        private ValueEditorPane editor;
        private UILabel disableLabel;

        public ParameterValueRenderer(int paraUseType) {
            this(ValueEditors.createValueEditorPaneWithUseType(paraUseType));
        }

        public ParameterValueRenderer(ValueEditorPane valueEditorPane) {
            disableLabel = new UILabel(DesignKit.i18nText("Fine-Design_Basic_Set_Paramete_Name"));
            disableLabel.setForeground(Color.pink);
            disableLabel.setHorizontalAlignment(SwingConstants.CENTER);

            editor = valueEditorPane;
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (table.isCellEditable(row, column)) {
                if (value == null) {
                    editor.populate(StringKit.EMPTY);
                } else {
                    editor.populate(value);
                }
                return editor;
            } else {
                return disableLabel;
            }
        }
    }

}
