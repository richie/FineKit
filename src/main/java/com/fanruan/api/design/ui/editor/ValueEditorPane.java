package com.fanruan.api.design.ui.editor;

import com.fr.design.editor.editor.Editor;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-27
 * 多指选择器
 * com.fr.design.editor.editor.Editor是一个可靠的API类
 */
public class ValueEditorPane extends com.fr.design.editor.ValueEditorPane {

    public ValueEditorPane(Editor[] editors) {
        super(editors);
    }

    public ValueEditorPane(Editor[] editors, String popupName, String textEditorValue) {
        super(editors, popupName, textEditorValue);
    }

    public ValueEditorPane(Editor[] editors, String popupName, String textEditorValue, int centerPaneWidth) {
        super(editors, popupName, textEditorValue, centerPaneWidth);
    }
}
