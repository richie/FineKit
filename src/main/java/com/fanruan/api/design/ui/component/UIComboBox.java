package com.fanruan.api.design.ui.component;

import com.fanruan.api.util.AssistKit;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.stable.Constants;
import com.fr.stable.StringUtils;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Vector;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 下拉框
 */
public class UIComboBox<T> extends com.fr.design.gui.icombobox.UIComboBox {

    public UIComboBox() {
        super();
    }

    public UIComboBox(ComboBoxModel<T> model) {
        super(model);
    }

    public UIComboBox(T[] items) {
        super(items);
    }

    public UIComboBox(Vector<T> items) {
        super(items);
    }

    /**
     * 重新设置下拉选项
     *
     * @param list 下拉选项的集合
     */
    public void refreshSelectableItems(List<T> list) {
        T el = (T) getSelectedItem();
        removeAllItems();
        for (T t : list) {
            addItem(t);
        }
        getModel().setSelectedItem(el);
    }

    public static class UIComboBoxEditor extends BasicComboBoxEditor {
        protected UITextField textField;
        private Object oldValue;

        public UIComboBoxEditor() {
            textField = new UITextField();
            textField.setRectDirection(Constants.RIGHT);
        }

        @Override
        public Component getEditorComponent() {
            return textField;
        }

        @Override
        public void setItem(Object anObject) {
            if (anObject != null) {
                textField.setText(anObject.toString());
                oldValue = anObject;
            } else {
                textField.setText(StringUtils.EMPTY);
            }

        }

        @Override
        public Object getItem() {
            Object newValue = textField.getText();
            if (oldValue != null && !(oldValue instanceof String)) {
                if (AssistKit.equals(newValue, oldValue.toString())) {
                    return oldValue;
                } else {
                    Class cls = oldValue.getClass();
                    try {
                        Method method = cls.getMethod("valueOf", new Class[]{String.class});
                        newValue = method.invoke(oldValue, new Object[]{textField.getText()});
                    } catch (Exception ignore) {

                    }
                }
            }
            return newValue;

        }

        @Override
        public void selectAll() {
            textField.selectAll();
            textField.requestFocus();
        }

        @Override
        public void addActionListener(ActionListener l) {
            textField.addActionListener(l);
        }

        @Override
        public void removeActionListener(ActionListener l) {
            textField.removeActionListener(l);
        }
    }
}
