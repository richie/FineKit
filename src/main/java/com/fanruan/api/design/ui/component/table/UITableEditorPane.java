package com.fanruan.api.design.ui.component.table;


import com.fanruan.api.design.ui.component.table.model.UITableModelAdapter;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 可增、删、改的表格控件
 */
public class UITableEditorPane<T> extends com.fr.design.gui.itableeditorpane.UITableEditorPane<T> {
    
    public UITableEditorPane(UITableModelAdapter<T> adapter) {
        super(adapter);
    }
    
    public UITableEditorPane(UITableModelAdapter<T> adapter, String leftLabelName) {
        super(adapter, leftLabelName);
    }
}
