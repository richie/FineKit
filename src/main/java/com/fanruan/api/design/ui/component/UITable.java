package com.fanruan.api.design.ui.component;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 表格控件
 */
public class UITable extends com.fr.design.gui.itable.UITable {

    public UITable() {
        super();
    }

    public UITable(int columnSize) {
        super(columnSize);
    }

    /**
     * @param values 一个列表，里面装有字符串数组，每个数组代表一行内容
     */
    public UITable(@NotNull List<Object[]> values) {
        super(values);
    }

    public UITable(int columnSize, boolean needAWTEventListener) {
        super(columnSize, needAWTEventListener);
    }


}
