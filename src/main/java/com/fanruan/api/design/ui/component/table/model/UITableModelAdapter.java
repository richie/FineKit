package com.fanruan.api.design.ui.component.table.model;

import com.fanruan.api.design.DesignKit;
import com.fanruan.api.design.ui.component.table.action.UITableEditAction;
import com.fanruan.api.util.IOKit;
import com.fr.design.mainframe.DesignerContext;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 
 */
public abstract class UITableModelAdapter<T> extends com.fr.design.gui.itableeditorpane.UITableModelAdapter<T> {

    protected UITableModelAdapter(String[] strings) {
        super(strings);
    }

    public abstract UITableEditAction[] createAction();


    protected abstract class AddTableRowAction extends UITableEditAction {

        public AddTableRowAction() {
            this.setName(DesignKit.i18nText("Fine-Design_Report_Insert"));
            this.setSmallIcon(IOKit.readIcon("/com/fr/base/images/cell/control/add.png"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            stopCellEditing();
        }

        public void checkEnabled() {

        }
    }

    protected abstract class EditAction extends UITableEditAction {

        public EditAction() {
            this.setName(DesignKit.i18nText("Fine-Design_Report_Edit"));
            this.setSmallIcon(IOKit.readIcon("/com/fr/design/images/control/edit.png"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            final int selectedRow = table.getSelectedRow();
            if (selectedRow > table.getRowCount() - 1 || selectedRow < 0) {
                JOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), DesignKit.i18nText("Fine-Design_Basic_No-Alternatives"));
                return;
            }
            stopCellEditing();

        }

    }
}
