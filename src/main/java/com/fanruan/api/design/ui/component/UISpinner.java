package com.fanruan.api.design.ui.component;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 数字滚动控件
 */
public class UISpinner  extends com.fr.design.gui.ispinner.UISpinner {

    public UISpinner(double minValue, double maxValue, double delta) {
        super(minValue, maxValue, delta);
    }

    public UISpinner(double minValue, double maxValue, double delta, double defaultValue) {
        super(minValue, maxValue, delta, defaultValue);
    }
}
