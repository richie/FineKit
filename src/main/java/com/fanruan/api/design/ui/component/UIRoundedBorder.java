package com.fanruan.api.design.ui.component;

import java.awt.*;

/**
 * 圆角形状的边框
 */
public class UIRoundedBorder extends com.fr.design.border.UIRoundedBorder {
    public UIRoundedBorder(Color color) {
        super(color);
    }

    public UIRoundedBorder(Color color, int thickness) {
        super(color, thickness);
    }

    public UIRoundedBorder(Color color, int thickness, int roundedCorners) {
        super(color, thickness, roundedCorners);
    }

    public UIRoundedBorder(int lineStyle, Color color, int roundedCorners) {
        super(lineStyle, color, roundedCorners);
    }
}
