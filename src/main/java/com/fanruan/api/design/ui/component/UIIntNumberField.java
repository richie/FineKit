package com.fanruan.api.design.ui.component;

/**
 * 整数控件，该控件只能输入整数数字
 */
public class UIIntNumberField extends com.fr.design.gui.itextfield.UIIntNumberField {

    public UIIntNumberField() {

    }

    /**
     * 获取整数数值
     *
     * @return 整数值
     */
    public int getInt() {
        return (int) getValue();
    }

    /**
     * 设置整数数值
     *
     * @param value 整数值
     */
    public void setInt(int value) {
        setValue(value);
    }
}
