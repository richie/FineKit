package com.fanruan.api.design.ui.component;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 文本域组件
 */
public class UITextArea extends com.fr.design.gui.itextarea.UITextArea {

    public UITextArea() {
        super();
    }

    public UITextArea(String text) {
        super(text);
    }

    public UITextArea(int rows, int columns) {
        super(rows, columns);
    }
}
