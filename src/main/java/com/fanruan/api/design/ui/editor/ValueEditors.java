package com.fanruan.api.design.ui.editor;

import com.fanruan.api.design.DesignKit;
import com.fanruan.api.design.ui.component.table.model.ParameterTableModel;
import com.fanruan.api.util.StringKit;
import com.fr.base.BaseFormula;
import com.fr.design.editor.editor.BooleanEditor;
import com.fr.design.editor.editor.ColumnRowEditor;
import com.fr.design.editor.editor.ColumnRowGroupEditor;
import com.fr.design.editor.editor.ColumnSelectedEditor;
import com.fr.design.editor.editor.ConstantsEditor;
import com.fr.design.editor.editor.CursorEditor;
import com.fr.design.editor.editor.DateEditor;
import com.fr.design.editor.editor.DoubleEditor;
import com.fr.design.editor.editor.Editor;
import com.fr.design.editor.editor.FormulaEditor;
import com.fr.design.editor.editor.IntegerEditor;
import com.fr.design.editor.editor.NoneEditor;
import com.fr.design.editor.editor.ParameterEditor;
import com.fr.design.editor.editor.SpinnerIntegerEditor;
import com.fr.design.editor.editor.TextEditor;
import com.fr.design.editor.editor.WidgetNameEditor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 工厂类，创建多种组合编辑器
 */
public class ValueEditors {

    /**
     * 创建带编辑器的ValueEditorPane
     *
     * @param editors 自定义的编辑器
     * @return 返回pane
     */
    public static ValueEditorPane createValueEditorPane(Editor<?>[] editors) {
        return createValueEditorPane(editors, StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 创建编辑器 名称 弹出的ValueEditorPane
     *
     * @param editors         编辑器
     * @param popupName       弹出的名字
     * @param textEditorValue 值
     * @return 返回pane
     */
    public static ValueEditorPane createValueEditorPane(Editor<?>[] editors, String popupName, String textEditorValue) {
        return new ValueEditorPane(editors, popupName, textEditorValue);
    }

    /**
     * 创建编辑器 名称 弹出的ValueEditorPane
     *
     * @param editors             编辑器
     * @param popupName           弹出的名字
     * @param textEditorValue     值
     * @param editor_center_width 编辑器主体的宽度
     * @return 返回pane
     */
    public static ValueEditorPane createValueEditorPane(Editor<?>[] editors, String popupName, String textEditorValue, int editor_center_width) {
        return new ValueEditorPane(editors, popupName, textEditorValue, editor_center_width);
    }

    /**
     * 创建基本的值编辑器面板
     *
     * @return 返回值编辑器面板
     */
    public static ValueEditorPane createBasicValueEditorPane() {
        return createValueEditorPane(basicEditors(), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 创建公式编辑器面板
     *
     * @return 返回公式编辑器面板
     */
    public static ValueEditorPane createFormulaValueEditorPane() {
        return createValueEditorPane(new Editor[]{new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"))},
                StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 创建基本的值编辑器面板
     *
     * @param editorCenterWidth 指定值编辑器的主体宽度
     * @return 返回值编辑器面板
     */
    public static ValueEditorPane createBasicValueEditorPane(int editorCenterWidth) {
        return createValueEditorPane(basicEditors(), StringKit.EMPTY, StringKit.EMPTY, editorCenterWidth);
    }

    /**
     * Process用的editorPane
     *
     * @return 值编辑器面板
     */
    public static ValueEditorPane createFormEditorPane() {
        return createValueEditorPane(formEditors(), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * URL使用的ValueEditorPane
     *
     * @param popupName       弹出的名字
     * @param textEditorValue 编辑器值
     * @return 值编辑器返回
     */
    public static ValueEditorPane createURLValueEditorPane(String popupName, String textEditorValue) {
        return createValueEditorPane(URLEditors(popupName, textEditorValue), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 创建日期的ValueEditorPane
     *
     * @param popupName       名字
     * @param textEditorValue 值
     * @return 值编辑器面板
     */
    public static ValueEditorPane createDateValueEditorPane(String popupName, String textEditorValue) {
        return createValueEditorPane(dateEditors(popupName, textEditorValue), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 带有所有编辑器的ValueEditorPane
     *
     * @return 值编辑器面板
     */
    public static ValueEditorPane createAllValueEditorPane() {
        return createValueEditorPane(allEditors(), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 创建不带公式面板的pane
     *
     * @return 编辑器面板
     */
    public static ValueEditorPane createBasicEditorWithoutFormulaPane() {
        return createValueEditorPane(basicEditorsWithoutFormula(), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 创建日期编辑器
     *
     * @return 值编辑器
     */
    public static ValueEditorPane createDateValueEditorPane() {
        return createValueEditorPane(dateEditors(), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 根据参数paraUseType 创建编辑器类型.
     *
     * @param paraUseType 参数类型
     * @return 值编辑器
     */
    public static ValueEditorPane createValueEditorPaneWithUseType(int paraUseType) {
        return createValueEditorPaneWithUseType(paraUseType, null);
    }

    public static ValueEditorPane createValueEditorPaneWithUseType(int paraUseType, Map<String, BaseFormula> hyperLinkEditorMap) {
        if (paraUseType == ParameterTableModel.NO_CHART_USE) {
            return createBasicValueEditorPane();
        } else if (paraUseType == ParameterTableModel.FORM_NORMAL_USE) {
            return createFormEditorPane();
        } else {
            return createChartHotValueEditorPane(hyperLinkEditorMap);
        }
    }

    /**
     * 图表用的参数编辑器的ValueEditorPane
     *
     * @param hyperLinkEditorMap 超链下拉参数类型
     * @return 值编辑器
     */
    public static ValueEditorPane createChartHotValueEditorPane(Map<String, BaseFormula> hyperLinkEditorMap) {
        return createValueEditorPane(chartHotEditors(hyperLinkEditorMap), StringKit.EMPTY, StringKit.EMPTY);
    }

    /**
     * 基础的一些ValueEditorPane所用到的Editors
     *
     * @return 值编辑器
     */
    public static Editor<?>[] basicEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new TextEditor(),
                new SpinnerIntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
                formulaEditor
        };
    }

    /**
     * 表单的一些编辑器.
     *
     * @return 值编辑器
     */
    public static Editor<?>[] formEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new TextEditor(),
                new IntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
                formulaEditor,
                new WidgetNameEditor(DesignKit.i18nText("Fine-Design_Report_Widget"))
        };
    }

    /**
     * 扩展单元格的一些编辑器
     *
     * @return 值编辑器
     */
    public static Editor<?>[] extendedEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new TextEditor(),
                new IntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
                formulaEditor,
                new ParameterEditor(),
                new ColumnRowEditor(DesignKit.i18nText("Fine-Design_Basic_Cell"))
        };
    }

    /**
     * 带单元格组的编辑器
     *
     * @return 值编辑器
     */
    public static Editor<?>[] extendedCellGroupEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new TextEditor(),
                new IntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
                formulaEditor,
                new ParameterEditor(),
                new ColumnRowEditor(DesignKit.i18nText("Fine-Design_Basic_Cell")),
                new ColumnRowGroupEditor(DesignKit.i18nText("Fine-Design_Basic_Cell_Group"))
        };
    }

    /**
     * 只有单元格和单元格组的编辑器
     *
     * @return 编辑器b
     */
    public static Editor<?>[] cellGroupEditor() {
        return new Editor[]{
                new ColumnRowEditor(DesignKit.i18nText("Fine-Design_Basic_Cell")),
                new ColumnRowGroupEditor(DesignKit.i18nText("Fine-Design_Basic_Cell_Group"))
        };
    }

    /**
     * URL的一些编辑器.
     *
     * @param popupName       名字
     * @param textEditorValue 值
     * @return 值编辑器
     */
    public static Editor<?>[] URLEditors(String popupName, String textEditorValue) {
        return new Editor[]{
                new NoneEditor(textEditorValue, StringKit.isEmpty(popupName) ? DesignKit.i18nText("Fine-Design_Basic_None") : popupName),
                new TextEditor()
        };
    }

    /**
     * 日期类型的一些编辑器
     *
     * @param popupName       名字
     * @param textEditorValue 值
     * @return 值编辑器
     */
    public static Editor<?>[] dateEditors(String popupName, String textEditorValue) {
        return new Editor[]{
                new NoneEditor(textEditorValue, StringKit.isEmpty(popupName) ? DesignKit.i18nText("Fine-Design_Basic_None") : popupName),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"))
        };
    }

    /**
     * 所有类型的编辑器
     *
     * @return 值编辑器
     */
    public static Editor<?>[] allEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new TextEditor(),
                new IntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
                formulaEditor,
                new ParameterEditor(),
                new ColumnRowEditor(DesignKit.i18nText("Fine-Design_Basic_Cell")),
                new ColumnSelectedEditor(),
        };
    }

    /**
     * 不带公式编辑器
     *
     * @return 编辑器不带公式
     */
    public static Editor<?>[] basicEditorsWithoutFormula() {
        return new Editor[]{
                new TextEditor(),
                new IntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
        };
    }

    /**
     * noCRnoColumn编辑器
     *
     * @return 编辑器
     */
    public static Editor<?>[] noCRnoColumnEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new TextEditor(),
                new IntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
                formulaEditor,
                new ParameterEditor(),
        };
    }

    /**
     * 数值编辑器
     *
     * @return 编辑器
     */
    public static Editor<?>[] numberEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new IntegerEditor(),
                new DoubleEditor(),
                formulaEditor,
                new ParameterEditor(),
        };
    }

    /**
     * 日期编辑器
     *
     * @return 编辑器
     */
    public static Editor<?>[] dateEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        return new Editor[]{
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                formulaEditor,
                new ParameterEditor(),
        };
    }

    /**
     * 存储的一些编辑器
     *
     * @return 存储过程的编辑器
     */
    public static Editor<?>[] StoreProcedureEditors() {
        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        formulaEditor.setEnabled(true);
        return new Editor[]{
                new CursorEditor(),
                new TextEditor(),
                new IntegerEditor(),
                new DoubleEditor(),
                new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")),
                new BooleanEditor(),
                formulaEditor
        };
    }

    /**
     * 图表热点的一些编辑器
     *
     * @return 值编辑器
     */
    public static Editor[] chartHotEditors(Map<String, BaseFormula> hyperLinkEditorMap) {
        List<Editor> list = createEditors4Chart(hyperLinkEditorMap);

        list.add(new TextEditor());
        list.add(new IntegerEditor());
        list.add(new DoubleEditor());
        list.add(new DateEditor(true, DesignKit.i18nText("Fine-Design_Basic_Date")));
        list.add(new BooleanEditor());

        FormulaEditor formulaEditor = new FormulaEditor(DesignKit.i18nText("Fine-Design_Basic_Parameter_Formula"));
        formulaEditor.setEnabled(true);
        list.add(formulaEditor);

        return list.toArray(new Editor[0]);
    }

    /**
     * 为图表创建编辑器.
     *
     * @return 值编辑器
     */
    private static List<Editor> createEditors4Chart(Map<String, BaseFormula> hyperLinkEditorMap) {
        List<Editor> lists = new ArrayList<>();
        if (hyperLinkEditorMap == null) {
            return lists;
        }
        Iterator<Map.Entry<String, BaseFormula>> entries = hyperLinkEditorMap.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, BaseFormula> entry = entries.next();
            ConstantsEditor editor = new ConstantsEditor(entry.getKey(), entry.getValue());
            editor.setEnabled(false);
            lists.add(editor);
        }
        return lists;
    }
}
