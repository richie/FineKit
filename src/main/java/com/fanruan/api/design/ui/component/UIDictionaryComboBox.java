package com.fanruan.api.design.ui.component;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 带字典数据的下拉框，显示值和实际值通过字典进行对应
 */
public class UIDictionaryComboBox<T> extends com.fr.design.gui.icombobox.UIDictionaryComboBox<T> {

    public UIDictionaryComboBox(T[] keys, String[] values) {
        super(keys, values);
    }
}
