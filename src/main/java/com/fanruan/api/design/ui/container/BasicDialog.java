package com.fanruan.api.design.ui.container;

import java.awt.*;

public class BasicDialog extends com.fr.design.dialog.BasicDialog {

    public BasicDialog(Dialog parent) {
        super(parent);
    }

    public BasicDialog(Dialog parent, BasicPane pane) {
        super(parent, pane);
    }

    public BasicDialog(Dialog parent, BasicPane pane, boolean isNeedButton) {
        super(parent, pane, isNeedButton);
    }

    public BasicDialog(Frame parent) {
        super(parent);
    }

    public BasicDialog(Frame parent, BasicPane pane) {
        super(parent, pane);
    }

    public BasicDialog(Frame parent, BasicPane pane, boolean isNedButtonPane) {
        super(parent, pane, isNedButtonPane);
    }

    @Override
    protected void setBasicDialogSize(Dimension dimension) {
        super.setBasicDialogSize(dimension);
    }

    @Override
    public void checkValid() throws Exception {

    }
}
