package com.fanruan.api.design.ui.component;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 数字文本框
 */
public class UINumberField  extends com.fr.design.gui.itextfield.UINumberField {

    /**
     * 默认的数字文本框，限制最大的输入整数位数为32位，限制最大的小数输入位数为16位
     */
    public UINumberField() {
        super();
    }

    /**
     * 默认的数字文本框，限制最大的输入整数位数为32位，限制最大的小数输入位数为16位
     * @param columns 最大的文本输入列数
     */
    public UINumberField(int columns) {
        super(columns);
    }

    /**
     * 限制整数和小数位数的数字文本框
     * @param maxIntegerLength 最大整数位数
     * @param maxDecimalLength 最大小数位数
     */
    public UINumberField(int maxIntegerLength, int maxDecimalLength) {
        super(maxIntegerLength, maxDecimalLength);
    }

    /**
     * 限制整数和小数位数、最大值以及最小值的数字文本框
     * @param maxIntegerLength 最大整数位数
     * @param maxDecimalLength 最大小数位数
     * @param minValue 最小值
     * @param maxValue 最大值
     */
    public UINumberField(int maxIntegerLength, int maxDecimalLength, double minValue, double maxValue) {
        super(maxIntegerLength, maxDecimalLength, minValue, maxValue);
    }
}
