package com.fanruan.api.design.ui.component;

import javax.swing.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 按钮组件
 */
public class UIButton extends com.fr.design.gui.ibutton.UIButton {

    public UIButton() {
        super();
    }

    public UIButton(String text) {
        super(text);
    }

    public UIButton(Icon icon) {
        super(icon);
    }

    public UIButton(Action action) {
        super(action);
    }

    public UIButton(String text, Icon icon) {
        super(text, icon);
    }
}
