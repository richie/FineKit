package com.fanruan.api.design.ui.component.code;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-28
 * 代码编辑器用到的常量
 */
public interface SyntaxConstants {

    /**
     * 普通文本编辑器
     */
    String SYNTAX_STYLE_NONE = "text/plain";

    /**
     * java编辑器
     */
    String SYNTAX_STYLE_JAVA = "text/java";

    /**
     * javascript编辑器
     */
    String SYNTAX_STYLE_JAVASCRIPT = "text/javascript";

    /**
     * sql编辑器
     */
    String SYNTAX_STYLE_SQL = "text/sql";

    /**
     * 公式编辑器
     */
    String SYNTAX_STYLE_FORMULA = "text/formula";
}
