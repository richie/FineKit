package com.fanruan.api.design.macro;

import java.awt.*;

/**
 * 设计器界面使用的一些常量
 */
public interface UIConstants {

    /**
     * 边框线颜色
     */
    Color LINE_COLOR = new Color(153, 153, 153);

    /**
     * 带标题的边框的颜色
     */
    Color TITLED_BORDER_COLOR = new Color(0xe8e8e9);

    /**
     * 圆角弧度
     */
    int ARC = 0;
}
