package com.fanruan.api.design.util;

import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.*;
import java.awt.*;

public class GUICoreKit {

    /**
     * 创建一个流式布局容器并将所有的组件添加进去
     *
     * @param args 组件
     * @return 容器
     */
    public static JPanel createFlowPane(Object... args) {
        return GUICoreUtils.createFlowPane(args);
    }

    /**
     * 产生一个流式布局的容器，并将所有的组件添加进去, flowAlignment是FlowLayout.LEFT, CENTER, RIGHT.
     *
     * @param comp          组件
     * @param flowAlignment 对齐方式
     * @return 容器
     */
    public static JPanel createFlowPane(Component comp, int flowAlignment) {
        return GUICoreUtils.createFlowPane(new Component[]{comp}, flowAlignment);
    }

    /**
     * 产生一个流式布局的容器，并将所有的组件添加进去, flowAlignment是FlowLayout.LEFT, CENTER, RIGHT.
     *
     * @param comps         组件
     * @param flowAlignment 对齐方式
     * @return 容器
     */
    public static JPanel createFlowPane(Component[] comps, int flowAlignment) {
        return GUICoreUtils.createFlowPane(comps, flowAlignment);
    }

    /**
     * 产生一个流式布局的容器，并将所有的组件添加进去, flowAlignment是FlowLayout.LEFT, CENTER, RIGHT.
     *
     * @param comps         组件
     * @param flowAlignment 对齐方式
     * @param hSpace        水平间隔
     * @return 容器
     */
    public static JPanel createFlowPane(Component[] comps, int flowAlignment, int hSpace) {
        return GUICoreUtils.createFlowPane(comps, flowAlignment, hSpace);
    }

    /**
     * 产生一个流式布局的容器，并将所有的组件添加进去, flowAlignment是FlowLayout.LEFT, CENTER, RIGHT,
     *
     * @param comps         组件
     * @param flowAlignment 对齐方式
     * @param hSpace        垂直间隔
     * @param vSpace        水平间隔
     * @return 容器
     */
    public static JPanel createFlowPane(Component[] comps, int flowAlignment, int hSpace, int vSpace) {
        return GUICoreUtils.createFlowPane(comps, flowAlignment, hSpace, vSpace);
    }

    /**
     * 生成一个边界布局的容器，并根据参数规则将组件添加进去
     *
     * @param components 面板中的组件，第一个组件位置在中间，第二个组件位置再东边，
     *                   第三个组件位置在南边，第四个组件位置在西边，第五个组件位置在北边
     * @return 容器
     */
    public static JPanel createBorderLayoutPane(Component[] components) {
        return GUICoreUtils.createBorderLayoutPane(components);
    }

    /**
     * 生成一个边界布局的容器，并根据参数规则将组件添加进去
     * <p>
     * GUICoreKit.createBorderLayoutPane(new JLabel(), BorderLayout.CENTER, new JButton(), BorderLayout.NORTH);
     * <p/>
     *
     * @param args 布局内部的元素，位置等
     * @return 具有边界布局的面板
     */
    public static JPanel createBorderLayoutPane(Object... args) {
        return GUICoreUtils.createBorderLayoutPane(args);
    }

    /**
     * 弹出菜单，在坐标为x,y处弹出内容为component的菜单
     *
     * @param menu      目录框
     * @param component 目录内组件
     * @param x         横坐标
     * @param y         纵坐标
     */
    public static void showPopupMenu(JPopupMenu menu, Component component, int x, int y) {
        GUICoreUtils.showPopupMenu(menu, component, x, y);
    }

    /**
     * 创建一个带有左空边框的面板
     *
     * @return 返回一个JPanel对象
     */
    public static JPanel createNormalFlowInnerContainerPane() {
        return FRGUIPaneFactory.createNormalFlowInnerContainer_M_Pane();
    }

    /**
     * 创建一个带有边框面板
     *
     * @return 返回一个JPanel对象
     */
    public static JPanel createBorderLayoutPane() {
        return FRGUIPaneFactory.createBorderLayout_S_Pane();
    }

    /**
     * 设置一个窗口居中
     */
    public static void centerWindow(Window win) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        Dimension winSize = win.getSize();

        if (winSize.height > screenSize.height) {
            winSize.height = screenSize.height;
        }
        if (winSize.width > screenSize.width) {
            winSize.width = screenSize.width;
        }
        win.setLocation((screenSize.width - winSize.width) / 2, (screenSize.height - winSize.height) / 2 - 20);
    }
}
