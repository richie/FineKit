package com.fanruan.api.report;

import com.fr.io.TemplateWorkBookIO;
import com.fr.main.TemplateWorkBook;
/**
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 * 模板操作相关工具类
 */
public class TemplateKit {
    /**
     * 通过相对路径读取模板
     *
     * @param relativePath 相对路径（相对reportlets目录）
     * @return 模板对象
     * @throws Exception 模板读取异常
     */
    public static TemplateWorkBook readTemplateByPath(String relativePath) throws Exception {
        return TemplateWorkBookIO.readTemplateWorkBook(
                relativePath);
    }
}