package com.fanruan.api.report.page;

import com.fr.main.workbook.ResultWorkBook;
import com.fr.page.PaperSettingProvider;
import com.fr.report.core.ReportUtils;

import java.util.List;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 * 分页预览相关工具类
 */
public class PageKit {

    /**
     * 获取报表的打印纸张设置信息
     * @param rb 结果报表
     * @return 打印纸张设置信息集合
     */
    public static PaperSettingProvider[] getPaperSettingListFromWorkBook(ResultWorkBook rb) {
        List<PaperSettingProvider> list = ReportUtils.getPaperSettingListFromWorkBook(rb);
        return list == null ? new PaperSettingProvider[0] : list.toArray(new PaperSettingProvider[0]);
    }
}
