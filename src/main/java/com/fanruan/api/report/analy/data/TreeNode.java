package com.fanruan.api.report.analy.data;

import com.fr.cache.list.IntList;

/**
 * 折叠行树节点对象
 */
public class TreeNode {
    private int self;//自身所在行
    private int parent;//父节点所在行
    private IntList sons;//子节点

    public TreeNode(int self, int parent, IntList sons) {
        this.self = self;
        this.parent = parent;
        this.sons = sons;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public void setSons(IntList sons) {
        this.sons = sons;
    }

    public void setSelf(int self) {
        this.self = self;
    }

    public int getParent() {
        return parent;
    }

    public int getSelf() {
        return self;
    }

    public IntList getSons() {
        return sons;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(self).append("#").append(parent);
        if (sons != null) {
            stringBuffer.append("#").append(sons.toString());
        }
        return stringBuffer.toString();
    }
}