package com.fanruan.api.report.analy;

import com.fanruan.api.report.analy.data.TreeNode;
import com.fr.cache.list.IntList;
import com.fr.form.ui.Widget;
import com.fr.main.workbook.ResultWorkBook;
import com.fr.report.cell.ResultCellElement;
import com.fr.report.cell.WidgetAttrElem;
import com.fr.report.report.Report;
import com.fr.report.report.ResultReport;
import com.fr.report.web.button.form.TreeNodeToggleButton;
import com.fr.report.worksheet.AnalysisRWorkSheet;
import com.fr.script.Calculator;
import com.fr.web.core.TreeHTMLWriter;
import com.fr.web.core.reportcase.WebElementReportCase;
import com.fr.web.output.html.chwriter.ViewCellWriter;
import com.fr.web.request.EmptyRepository;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 * 数据分析相关工具类
 */
public class AnalyKit {
    /**
     * 折叠树模板，将结果报表行转成一个树形数据结构
     *
     * @param book  结果报表
     * @param index sheet索引
     * @return 树节点集合
     */
    public static Map<Integer, TreeNode> generateResultBookTree(ResultWorkBook book, int index) {
        ResultReport resultWS = book.getResultReport(index);
        if (!(resultWS instanceof AnalysisRWorkSheet)) {
            //只有分析预览支持折叠树
            throw new UnsupportedOperationException();
        }
        AnalysisRWorkSheet analysisRWorkSheet = (AnalysisRWorkSheet) resultWS;

        Calculator c = Calculator.createCalculator();
        c.setAttribute(Report.KEY, analysisRWorkSheet);
        TreeHTMLWriter htmlWriter = new TreeHTMLWriter();
        ViewCellWriter cellHtmlWriter = new ViewCellWriter(new EmptyRepository(), 1, resultWS.getReportSettings(), true);
        htmlWriter.writeReportToHtml(new WebElementReportCase(analysisRWorkSheet, new EmptyRepository()), 1, cellHtmlWriter, new EmptyRepository(), "");
        cellHtmlWriter.dealWithAllTreeNodeRelation(c);

        return generateNodeTree(analysisRWorkSheet);
    }

    private static Map<Integer, TreeNode> generateNodeTree(AnalysisRWorkSheet resultWS) {
        int rowSize = resultWS.getRowCount();
        Map<Integer, TreeNode> nodeMap = new HashMap<Integer, TreeNode>();

        for (int rowIndex = 0; rowIndex < rowSize; rowIndex++) {//遍历行
            ResultCellElement treeNodeCell = findToggleCell(resultWS, rowIndex);
            if (treeNodeCell != null) {
                Widget widget = ((WidgetAttrElem) treeNodeCell).getWidget();
                IntList sonList = ((TreeNodeToggleButton) widget).getRelativeIndexList();
                if (sonList != null && sonList.size() > 1) {
                    if (sonList.get(0) == -1) {
                        //折叠行
                        if (nodeMap.containsKey(treeNodeCell.getRow())) {
                            continue;
                        }
                        buildNodeMap(resultWS, treeNodeCell, nodeMap, -1);
                    } else {
                        //折叠列 暂不处理
                    }
                }
            }
        }
        return nodeMap;
    }

    private static ResultCellElement findToggleCell(AnalysisRWorkSheet reportCase, int rowIndex) {
        Iterator cellIterator = reportCase.getRow(rowIndex);
        while (cellIterator.hasNext()) {
            ResultCellElement tmpCell = (ResultCellElement) cellIterator.next();
            if (tmpCell instanceof WidgetAttrElem) {
                if (((WidgetAttrElem) tmpCell).getWidget() instanceof TreeNodeToggleButton) {
                    return tmpCell;
                }
            }
        }
        return null;
    }

    private static void buildNodeMap(AnalysisRWorkSheet reportCase, ResultCellElement cellElment, Map<Integer, TreeNode> nodeMap, int parent) {
        if (cellElment == null) {
            return;
        }
        TreeNodeToggleButton toggleButton = (TreeNodeToggleButton) ((WidgetAttrElem) cellElment).getWidget();
        int self = cellElment.getRow();
        IntList sonList = toggleButton.getRelativeIndexList();
        if (sonList != null && sonList.size() > 1) {
            if (sonList.get(0) == -1) {//折叠行
                nodeMap.put(self, new TreeNode(self, parent, sonList));
                int size = sonList.size();
                for (int i = 0; i < size; i++) {
                    buildNodeMap(reportCase, findToggleCell(reportCase, sonList.get(i)), nodeMap, self);
                }
            }
        }
    }
}