package com.fanruan.api.macro;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-09-02
 * 常量
 */
public interface Constants {

    int CENTER = 0;

    int TOP = 1;


    int LEFT = 2;


    int BOTTOM = 3;


    int RIGHT = 4;
}
