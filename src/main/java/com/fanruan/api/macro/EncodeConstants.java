package com.fanruan.api.macro;


/**
 * 编码常量
 */
public interface EncodeConstants {

    /**
     * utf-8编码
     */
    String ENCODING_UTF_8 = com.fr.stable.EncodeConstants.ENCODING_UTF_8;

    /**
     * gbk编码
     */
    String ENCODING_GBK = com.fr.stable.EncodeConstants.ENCODING_GBK;

    /**
     * big5编码，一般用于繁体中文
     */
    String ENCODING_BIG5 = com.fr.stable.EncodeConstants.ENCODING_BIG5;

    /**
     * ios-8859-1编码
     */
    String ENCODING_ISO_8859_1 = com.fr.stable.EncodeConstants.ENCODING_ISO_8859_1;

    /**
     * utf16编码
     */
    String ENCODING_UTF_16 = com.fr.stable.EncodeConstants.ENCODING_UTF_16;

    /**
     * euc-jp编码
     */
    String ENCODING_EUC_JP = com.fr.stable.EncodeConstants.ENCODING_EUC_JP;

    /**
     * euc-kr编码
     */
    String ENCODING_EUC_KR = com.fr.stable.EncodeConstants.ENCODING_EUC_KR;

    /**
     * cp850编码
     */
    String ENCODING_CP850 = com.fr.stable.EncodeConstants.ENCODING_CP850;

    /**
     * 所有编码的集合
     */
    String[] ENCODING_ARRAY = {
            ENCODING_GBK,
            ENCODING_BIG5,
            ENCODING_ISO_8859_1,
            ENCODING_UTF_8,
            ENCODING_UTF_16,
            ENCODING_EUC_JP,
            ENCODING_EUC_KR,
            ENCODING_CP850
    };
}
