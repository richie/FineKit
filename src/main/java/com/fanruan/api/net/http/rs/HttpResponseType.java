package com.fanruan.api.net.http.rs;

import com.fr.third.org.apache.http.HttpEntity;
import com.fr.third.org.apache.http.client.methods.CloseableHttpResponse;
import com.fr.third.org.apache.http.client.methods.HttpUriRequest;
import com.fr.third.org.apache.http.client.protocol.HttpClientContext;
import com.fr.third.org.apache.http.impl.client.CloseableHttpClient;
import com.fr.third.org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-29
 */
public interface HttpResponseType<T> {

    /**
     * 处理http响应
     *
     * @param client  客户端
     * @param url     地址
     * @param request 请求
     * @param charset 字符集
     * @return 处理之后的响应
     * @throws IOException 异常
     */
    @Deprecated
    T result(CloseableHttpClient client, String url, HttpUriRequest request, String charset) throws IOException;

    /**
     * 处理http响应
     *
     * @param response 响应
     * @param charset  字符集
     * @return 处理之后的响应
     * @throws IOException 异常
     */
    T result(CloseableHttpResponse response, String charset) throws IOException;

    HttpResponseType<String> TEXT = new HttpResponseType<String>() {

        @Override
        public String result(CloseableHttpClient client, String url, HttpUriRequest request, String charset) throws IOException {
            CloseableHttpResponse response = client.execute(request, HttpClientContext.create());
            return result(response, charset);
        }

        @Override
        public String result(CloseableHttpResponse response, String charset) throws IOException {
            try {
                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity, charset);
                EntityUtils.consume(entity);
                return result;
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }
    };

    HttpResponseType<ByteArrayInputStream> STREAM = new HttpResponseType<ByteArrayInputStream>() {

        @Override
        public ByteArrayInputStream result(CloseableHttpClient client, String url, HttpUriRequest request, String charset) throws IOException {
            CloseableHttpResponse response = client.execute(request, HttpClientContext.create());
            return result(response, charset);
        }

        @Override
        public ByteArrayInputStream result(CloseableHttpResponse response, String charset) throws IOException {
            InputStream in = null;
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    in = entity.getContent();
                    byte[] buff = new byte[8000];
                    int bytesRead;
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    while ((bytesRead = in.read(buff)) != -1) {
                        bao.write(buff, 0, bytesRead);
                    }
                    byte[] data = bao.toByteArray();
                    return new ByteArrayInputStream(data);
                }
                return null;
            } finally {
                if (response != null) {
                    response.close();
                }
                if (in != null) {
                    in.close();
                }
            }
        }
    };
}