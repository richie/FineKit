package com.fanruan.api.net.http.rs;

import com.fr.third.org.apache.http.HttpEntity;
import com.fr.third.org.apache.http.client.methods.CloseableHttpResponse;
import com.fr.third.org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * 文本响应解析器
 *
 * @author vito
 * @date 2019-07-14
 */
public class TextResponseHandle extends BaseHttpResponseHandle<String> {

    public static final TextResponseHandle DEFAULT = new TextResponseHandle();

    public TextResponseHandle() {
    }

    public TextResponseHandle(String encoding) {
        super(encoding);
    }

    @Override
    public String parse(CloseableHttpResponse response) throws IOException {
        try {
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity, getEncoding());
            EntityUtils.consume(entity);
            return result;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }
}
