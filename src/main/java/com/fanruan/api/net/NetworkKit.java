package com.fanruan.api.net;

import com.fr.data.NetworkHelper;
import org.jetbrains.annotations.Nullable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-15
 * 网络请求参数相关的工具类
 */
public class NetworkKit {

    /**
     * 生成一个打印输出器
     *
     * @param res HTTP响应
     * @return 打印输出器
     * @throws java.io.IOException 如果无法创建输出器则抛出此异常
     */
    public static PrintWriter createPrintWriter(HttpServletResponse res) throws IOException {
        return NetworkHelper.createPrintWriter(res);
    }

    /**
     * 生成一个打印输出器
     *
     * @param res         HTTP响应
     * @param charsetName 编码
     * @return 打印输出器
     * @throws java.io.IOException 如果无法创建输出器则抛出此异常
     */
    public static PrintWriter createPrintWriter(HttpServletResponse res, String charsetName) throws IOException {
        return NetworkHelper.createPrintWriter(res, charsetName);
    }

    /**
     * 写出指定的模板
     *
     * @param resource 模板路径
     * @param response HTTP响应
     * @param map      用于替换模板中参数的的参数集
     * @throws java.io.IOException 如果无法创建输出器则抛出此异常
     */
    public static void writeOutTemplate(String resource, HttpServletResponse response, Map<String, Object> map) throws IOException {
        NetworkHelper.writeOutTemplate(resource, response, map);
    }

    /**
     * 把HTTP请求中指定名字的参数值转化为整数，参数为空或不是整数则返回-1
     *
     * @param req      HTTP请求
     * @param paraName 参数名
     * @return 整型参数值
     */
    public static int getHTTPRequestIntParameter(HttpServletRequest req, String paraName) {
        return NetworkHelper.getHTTPRequestIntParameter(req, paraName);
    }

    /**
     * 把HTTP请求中指定名字的参数值转化为整数
     *
     * @param req          HTTP请求
     * @param paraName     参数名
     * @param defaultValue 默认值
     * @return 返回req中参数的整数值。参数为空或不是整数则返回defaultValue
     */
    public static int getHTTPRequestIntParameter(HttpServletRequest req, String paraName, int defaultValue) {
        return NetworkHelper.getHTTPRequestIntParameter(req, paraName, defaultValue);
    }

    /**
     * 从http请求中获取sessionID
     *
     * @param req HTTP请求
     * @return session编号
     */
    public static @Nullable String getHTTPRequestSessionIDParameter(HttpServletRequest req) {
        return NetworkHelper.getHTTPRequestSessionIDParameter(req);
    }

    /**
     * 把HTTP请求中指定名字的参数值转化为布尔值
     *
     * @param req      HTTP请求
     * @param paraName 参数名
     * @return 布尔类型的参数值
     */
    public static boolean getHTTPRequestBoolParameter(HttpServletRequest req, String paraName) {
        return NetworkHelper.getHTTPRequestBoolParameter(req, paraName);
    }

    /**
     * 获取HTTP请求中指定名字的参数值
     *
     * @param req      HTTP请求
     * @param paraName 参数名
     * @return 字符型参数值
     */
    public static @Nullable String getHTTPRequestParameter(HttpServletRequest req, String paraName) {
        return NetworkHelper.getHTTPRequestParameter(req, paraName);
    }

    /**
     * 获取第一个不为空的参数
     *
     * @param req       HTTP请求
     * @param paraNames 参数列表
     * @return 字符型参数值
     */
    public static @Nullable String getHTTPRequestParameter(HttpServletRequest req, String... paraNames) {
        return NetworkHelper.getHTTPRequestParameter(req, paraNames);
    }
}
