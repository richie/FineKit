package com.fanruan.api.net;

import com.fr.decision.webservice.url.alias.URLAlias;
import com.fr.decision.webservice.url.alias.URLAliasFactory;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-19
 * 路由工具类
 * 宽泛匹配：/foo 和 /aaa/bbb/foo 是匹配的
 */
public class URLAliasKit {

    /**
     * 创建路由的别名
     *
     * @param aliasPath 别名
     * @param targetURL 原始路由
     * @return 别名对象
     */
    public static URLAlias createRawAlias(String aliasPath, String targetURL) {
        return URLAliasFactory.createRawAlias(aliasPath, targetURL);
    }

    /**
     * 创建路由的别名
     *
     * @param aliasPath 别名
     * @param targetURL 原始路由
     * @param wideRange 是否宽泛匹配
     * @return 别名对象
     */
    public static URLAlias createRawAlias(String aliasPath, String targetURL, boolean wideRange) {
        return URLAliasFactory.createRawAlias(aliasPath, targetURL, wideRange);
    }

    /**
     * 创建决策平台的路由别名
     *
     * @param aliasPath         别名
     * @param decisionTargetURL 原始路由地址
     * @return 加了决策平台servlet名字前缀的别名对象
     */
    public static URLAlias createDecisionAlias(String aliasPath, String decisionTargetURL) {
        return URLAliasFactory.createDecisionAlias(aliasPath, decisionTargetURL);
    }

    /**
     * 创建决策平台的路由别名
     *
     * @param aliasPath         别名
     * @param decisionTargetURL 原始路由地址
     * @param wideRange         是否宽泛匹配
     * @return 加了决策平台servlet名字前缀的别名对象
     */
    public static URLAlias createDecisionAlias(String aliasPath, String decisionTargetURL, boolean wideRange) {
        return URLAliasFactory.createDecisionAlias(aliasPath, decisionTargetURL, wideRange);
    }

    /**
     * 创建插件的路由别名
     *
     * @param aliasPath  别名
     * @param pluginPath 插件请求地址
     * @return 别名对象
     */
    public static URLAlias createPluginAlias(String aliasPath, String pluginPath) {
        return URLAliasFactory.createPluginAlias(aliasPath, pluginPath);
    }

    /**
     * 创建插件的路由别名
     *
     * @param aliasPath  别名
     * @param pluginPath 插件请求地址
     * @param isPublic   是否为公开可访问的地址
     * @return 别名对象
     */
    public static URLAlias createPluginAlias(String aliasPath, String pluginPath, boolean isPublic) {
        return URLAliasFactory.createPluginAlias(aliasPath, pluginPath, isPublic);
    }

    /**
     * 创建插件的路由别名
     *
     * @param aliasPath  别名
     * @param pluginPath 插件请求地址
     * @param isPublic   是否为公开可访问的地址
     * @param wideRange  是否宽泛匹配
     * @return 别名对象
     */
    public static URLAlias createPluginAlias(String aliasPath, String pluginPath, boolean isPublic, boolean wideRange) {
        return URLAliasFactory.createPluginAlias(aliasPath, pluginPath, isPublic, wideRange);
    }
}
