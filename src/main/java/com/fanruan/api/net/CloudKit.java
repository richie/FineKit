package com.fanruan.api.net;

import com.fr.general.CloudCenter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 * 云中心交互工具类
 */
public class CloudKit {


    /**
     * 从云中心获取值
     *
     * @param kind           键
     * @param defaultSiteUrl 默认值
     * @return 键对应的值，如果没有值则返回默认值
     */
    public static @Nullable String acquireUrlByKind(@NotNull String kind, @NotNull String defaultSiteUrl) {
        return CloudCenter.getInstance().acquireUrlByKind(kind, defaultSiteUrl);
    }

    /**
     * 从云中心获取值
     *
     * @param kind 键
     * @return 键对应的值，如果没有值则返回空
     */
    public static @Nullable String acquireUrlByKind(@NotNull String kind) {
        return CloudCenter.getInstance().acquireUrlByKind(kind);
    }
}
