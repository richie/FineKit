package com.fanruan.api.function;

import com.fanruan.api.function.shell.FineFunc;
import com.fr.file.FunctionManager;
import com.fr.script.Calculator;
import com.fr.stable.StringUtils;
import com.fr.stable.script.FunctionDef;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 * 函数相关工具类
 */
public class FunctionKit {
    /**
     * 新增一个自定义函数
     *
     * @param fineFunc 自定义函数对象
     */
    public static void addFunc(FineFunc fineFunc) {
        if (fineFunc == null) {
            return;
        }
        if (Calculator.createCalculator().resolveMethod(fineFunc.getName()) == null) {
            FunctionManager.getInstance().addFunctionDef(new FunctionDef(fineFunc.getName(), fineFunc.getDescription(), fineFunc.getClassName()));
        }
    }

    /**
     * 根据自定义函数类路径移除一个自定义函数
     *
     * @param className 自定义函数类路径
     */
    public static void removeFunc(String className) {
        if (StringUtils.isEmpty(className)) {
            return;
        }
        int count = FunctionManager.getInstance().getFunctionDefCount();
        List<FunctionDef> functionDefList = new ArrayList<FunctionDef>(count);
        for (int i = 0; i < count; i++) {
            FunctionDef functionDef = FunctionManager.getInstance().getFunctionDef(i);
            if (!className.equalsIgnoreCase(functionDef.getClassName())) {
                functionDefList.add(functionDef);
            }
        }
        if (functionDefList.size() == count) {
            return;
        }
        FunctionManager.getInstance().clearAllFunctionDef();
        for (FunctionDef fun : functionDefList) {
            FunctionManager.getInstance().addFunctionDef(fun);
        }
    }
}