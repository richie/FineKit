package com.fanruan.api.function.shell;

import com.fr.stable.script.FunctionDef;

/**
 * ClassName FineFunc
 *
 * @Author zack
 * @Date 2019/8/23
 * @Version 10.0
 * 自定义函数实体
 */
public class FineFunc extends FunctionDef {
    public FineFunc() {
    }

    public FineFunc(String name, String descrption) {
        super(name, descrption);
    }

    public FineFunc(String name, String description, String className) {
        super(name, description, className);
    }
}