package com.fanruan.api;

import java.io.IOException;
import java.util.Properties;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class FineKit {

    private static String version;
    private static String build;

    static {
        Properties properties = new Properties();
        try {
            properties.load(FineKit.class.getResourceAsStream("/version.properties"));
            version = properties.getProperty("version");
            build = properties.getProperty("build");
        } catch (IOException ignore) {
            version = "10.0";
            build = "20190815";
        }
    }

    /**
     * 获取开发者工具套件的版本信息
     * @return 版本信息
     */
    public static String version() {
        return version;
    }

    /**
     * 获取开发者工具套件的构建日期信息，该信息一般作为某个API是否可用的依据
     * @return 构建日期
     */
    public static String build() {
        return build;
    }
}
