package com.fanruan.api.util;

import com.fanruan.api.util.trans.BaseSmsBody;
import com.fanruan.api.util.trans.EmailBody;
import com.fr.base.EmailManager;
import com.fr.base.sms.SMSManager;

/**
 * @author zack
 * @date 2019/8/23
 * @version 10.0
 * fine kit for data transmission.(eg. email or sms...)
 * 数据发送工具类（比如：邮件发送，短信发送）
 */
public class TransmissionKit {
    /**
     * 服务端是否支持短信服务
     *
     * @return 支持返回true 否则false
     */
    public static boolean isSmsFuncSupport() {
        return SMSManager.getInstance().isSMSFuncSupport();
    }

    /**
     * 发送短信
     *
     * @param baseSmsBody 短信实体
     * @return 发送成功返回true 否则false
     * @throws Exception
     */
    public static boolean sendSms(BaseSmsBody baseSmsBody) throws Exception {
        return baseSmsBody.send();
    }

    /**
     * 发送邮件
     *
     * @param emailBody 邮件实体
     * @return 发送成功返回true否则false
     * @throws Exception
     */
    public static boolean sendEmail(EmailBody emailBody) throws Exception {
        EmailManager.getInstance().send(
                emailBody.getToAddress(),
                emailBody.getCcAddress(),
                emailBody.getBccAddress(),
                emailBody.getFromAddress(),
                emailBody.getSubject(),
                emailBody.getBodyContent(),
                emailBody.getAttaches(),
                emailBody.getFormat(),
                emailBody.getContentAttaches(),
                emailBody.getSessionId()
        );
        return true;
    }
}