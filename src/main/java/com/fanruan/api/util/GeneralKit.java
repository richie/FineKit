package com.fanruan.api.util;

import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralUtils;
import com.fr.third.guava.base.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class GeneralKit {
    /**
     * 返回系统的首选MAC地址
     *
     * @return 表示系统MAC地址的字符串
     */
    public static @NotNull String getMacAddress() throws IOException {
        return GeneralUtils.getMacAddress();
    }

    /**
     * 任意对象转换为文本值
     * @param obj 待转换的对象
     * @return 文本值
     */
    public static @NotNull String objectToString(Object obj) {
        return GeneralUtils.objectToString(obj);
    }

    /**
     * 任意对象转换为数值，如果无法转换为数值，就返回0
     * @param obj 待转换的对象
     * @return 数值
     */
    public static @NotNull Number objectToNumber(Object obj) {
        return GeneralUtils.objectToNumber(obj);
    }

    /**
     * 任意对象转换为数值，如果无法转换为数值，根据returnNull参数决定，返回0或者返回null
     * @param obj 待转换的对象
     * @param returnNull 无法转换时，是否返回null
     * @return 数值对象或者null
     */
    public static @Nullable Number objectToNumber(Object obj, boolean returnNull) {
        return GeneralUtils.objectToNumber(obj, returnNull);
    }

    /**
     * 读取jar的版本号
     * @return 返回jar版本号
     */
    public static @Nullable String readBuildNO() {
        return GeneralUtils.readBuildNO();
    }

    /**
     * 返回两个对象比较的大小
     * @param obj1 源对象
     * @param obj2 待比较对象
     * @return 若相等返回0，obj1小于obj2返回负数，否则返回正数
     */
    public static int compare(Object obj1, Object obj2) {
        return ComparatorUtils.compare(obj1, obj2);
    }
}
