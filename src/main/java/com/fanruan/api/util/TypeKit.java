package com.fanruan.api.util;

public class TypeKit {

    /**
     * 判读指定的类是否是另一个类的子类
     *
     * @param current     指定的类
     * @param targetClass 另一个类
     * @return 如果当前类是目标类的子类则返回true，否则返回false
     */
    public static boolean classInstanceOf(Class<?> current, Class<?> targetClass) {
        if (current == null || targetClass == null) {
            return false;
        }
        return targetClass.isAssignableFrom(current);
    }

    /**
     * 判读指定的类是否是另一个类的子类
     *
     * @param object 指定的类
     * @param clazz  另一个类
     * @return 如果指定类是另一个类的子类则返回true，否则返回false
     */
    public static boolean objectInstanceOf(Object object, Class clazz) {
        if (object == null || clazz == null) {
            return false;
        }
        return clazz.isInstance(object);
    }
}
