package com.fanruan.api.util;

import com.fr.base.TemplateUtils;

import java.util.Map;

/**
 * 文本动态渲染工具类
 */
public class RenderKit {

    /**
     * 根据参数, 来计算文档字符串
     * @param text 待渲染的文本模板
     * @param map 参数集合
     * @return 返回渲染后的文本
     * @throws Exception 渲染过程中出现错误则抛出此异常
     */
    public static String renderParameter4Tpl(String text, Map<String, Object> map) throws Exception {
        return TemplateUtils.renderParameter4Tpl(text, map);
    }
}
