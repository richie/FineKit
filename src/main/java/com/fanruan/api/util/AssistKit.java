package com.fanruan.api.util;

import com.fr.stable.AssistUtils;

public class AssistKit {
    /**
     * 比较两个双精度浮点型数据是否相等
     * @param parm1
     * @param parm2
     * @return 相等返回true 错误返回false
     */
    public static boolean equals(double parm1, double parm2) {
        return AssistUtils.equals(parm1, parm2);
    }

    /**
     * 比较两个单精度浮点型数据是否相等
     * @param parm1
     * @param parm2
     * @return 相等返回true 错误返回false
     */
    public static boolean equals(float parm1, float parm2) {
        return AssistUtils.equals(parm1, parm2);
    }

    /**
     * 判断两个Object类型数据是否相等
     * @param parm1
     * @param parm2
     * @return 相等返回true 错误返回false
     */
    public static boolean equals(Object parm1, Object parm2) {
        return AssistUtils.equals(parm1, parm2);
    }

    /**
     * 判断两个int类型数据是否相等
     * @param parm1
     * @param parm2
     * @return 相等返回0，如果parm1大于parm2 返回1否则返回-1
     */
    public static int compare(int parm1, int parm2) {
        return AssistUtils.compare(parm1, parm2);
    }

    /**
     * 判断两个long类型数据是否相等
     * @param parm1
     * @param parm2
     * @return 相等返回0，如果parm1大于parm2 返回1否则返回-1
     */
    public static int compare(long parm1, long parm2) {
        return AssistUtils.compare(parm1, parm2);
    }

    /**
     * 生成对象的哈希码值
     * @param parm
     * @return 返回对象的哈希码值
     */
    public static int hashCode(Object... parm) {
        return AssistUtils.hashCode(parm);
    }

    /**
     * 返回反映这个对象的字符串
     * @param parm
     * @return 返回反映这个对象的字符串
     */
    public static String toString(Object parm) {
        return AssistUtils.toString(parm);
    }

    /**
     *
     * @param parm1
     * @param parm2
     * @return
     */
//    public static String toString(Object parm1, String... parm2) {
//        return AssistUtils.toString(parm1, parm2);
//    }


}
