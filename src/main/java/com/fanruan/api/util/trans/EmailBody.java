package com.fanruan.api.util.trans;

import com.fr.base.EmailAttachment;
import org.jetbrains.annotations.Nullable;

/**
 * 邮件体
 *
 * @author vito
 * @date 2019-07-24
 */
public class EmailBody {
    private String toAddress;
    private String ccAddress;
    private String bccAddress;
    private String fromAddress;
    private String subject;
    private String bodyContent;
    private EmailAttachment[] attaches;
    private String format;
    private EmailAttachment[] contentAttaches;
    private String sessionId;

    private EmailBody(Builder builder) {
        this.toAddress = builder.toAddress;
        this.ccAddress = builder.ccAddress;
        this.bccAddress = builder.bccAddress;
        this.fromAddress = builder.fromAddress;
        this.subject = builder.subject;
        this.bodyContent = builder.bodyContent;
        this.attaches = builder.attaches;
        this.format = builder.format;
        this.contentAttaches = builder.contentAttaches;
        this.sessionId = builder.sessionId;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getToAddress() {
        return toAddress;
    }

    public String getCcAddress() {
        return ccAddress;
    }

    public String getBccAddress() {
        return bccAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public String getSubject() {
        return subject;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public EmailAttachment[] getAttaches() {
        return attaches;
    }

    public String getFormat() {
        return format;
    }

    public EmailAttachment[] getContentAttaches() {
        return contentAttaches;
    }

    public String getSessionId() {
        return sessionId;
    }

    public static final class Builder {
        private String toAddress;
        private String ccAddress;
        private String bccAddress;
        private String fromAddress;
        private String subject;
        private String bodyContent;
        private EmailAttachment[] attaches;
        private String format;
        private EmailAttachment[] contentAttaches;
        private String sessionId;

        private Builder() {
        }

        public EmailBody build() {
            if (toAddress == null) {
                throw new IllegalArgumentException("toAddress should not be null");
            }
            if (subject == null) {
                throw new IllegalArgumentException("subject should not be null");
            }
            if (bodyContent == null) {
                throw new IllegalArgumentException("bodyContent should not be null");
            }
            return new EmailBody(this);
        }

        /**
         * 设置收件人地址
         *
         * @param toAddress 收件人地址
         * @return 建造者
         */
        public Builder toAddress(String toAddress) {
            this.toAddress = toAddress;
            return this;
        }

        /**
         * 设置抄送地址
         *
         * @param ccAddress 抄送地址
         * @return 建造者
         */
        public Builder ccAddress(@Nullable String ccAddress) {
            this.ccAddress = ccAddress;
            return this;
        }

        /**
         * 设置密送地址
         *
         * @param bccAddress 密送地址
         * @return 建造者
         */
        public Builder bccAddress(@Nullable String bccAddress) {
            this.bccAddress = bccAddress;
            return this;
        }

        /**
         * 设置发件人地址
         *
         * @param fromAddress 发件人地址
         * @return 建造者
         */
        public Builder fromAddress(@Nullable String fromAddress) {
            this.fromAddress = fromAddress;
            return this;
        }

        /**
         * 设置主题
         *
         * @param subject 主题
         * @return 建造者
         */
        public Builder subject(String subject) {
            this.subject = subject;
            return this;
        }

        /**
         * 设置正文
         *
         * @param bodyContent 正文
         * @return 建造者
         */
        public Builder bodyContent(String bodyContent) {
            this.bodyContent = bodyContent;
            return this;
        }

        /**
         * 设置附件
         *
         * @param attaches 附件
         * @return 建造者
         */
        public Builder attaches(@Nullable EmailAttachment[] attaches) {
            this.attaches = attaches;
            return this;
        }

        /**
         * 设置格式
         *
         * @param format 格式
         * @return 建造者
         */
        public Builder format(@Nullable String format) {
            this.format = format;
            return this;
        }

        /**
         * 设置邮件正文显示的附件
         *
         * @param contentAttaches 邮件正文显示的附件
         * @return 建造者
         */
        public Builder contentAttaches(@Nullable EmailAttachment[] contentAttaches) {
            this.contentAttaches = contentAttaches;
            return this;
        }

        /**
         * 设置报表的session id
         *
         * @param sessionId 报表的session id
         * @return 建造者
         */
        public Builder sessionId(@Nullable String sessionId) {
            this.sessionId = sessionId;
            return this;
        }
    }
}
