package com.fanruan.api.util.trans;

import com.fr.base.sms.SMSManager;
import com.fr.json.JSONObject;
import org.jetbrains.annotations.Nullable;

/**
 * 单个短信体
 *
 * @author vito
 * @date 2019-07-24
 */
public class SingleSmsBody extends BaseSmsBody {
    private String mobile;
    private JSONObject para;
    private String receiver;
    private boolean needRecord;

    private SingleSmsBody() {
    }

    /**
     * 获取收集号码
     *
     * @return 手机号码
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 获取参数
     *
     * @return 模板参数
     */
    public JSONObject getPara() {
        return para;
    }

    /**
     * 获取接收者
     *
     * @return 接收者
     */
    @Nullable
    public String getReceiver() {
        return receiver;
    }

    /**
     * 是否需要记录
     *
     * @return 是否需要记录
     */
    public boolean isNeedRecord() {
        return needRecord;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public boolean send() throws Exception {
        return SMSManager.getInstance().sendSMS(
                getTemplateCode(),
                getMobile(),
                getPara(),
                getReceiver(),
                isNeedRecord());
    }

    public static final class Builder {
        private String templateCode;
        private String mobile;
        private JSONObject para;
        private String receiver;
        private boolean needRecord;

        private Builder() {
        }

        /**
         * 设置模板代码
         *
         * @param templateCode 模板代码
         * @return builder
         */
        public Builder templateCode(String templateCode) {
            this.templateCode = templateCode;
            return this;
        }

        /**
         * 设置手机号码
         *
         * @param mobile 手机号码
         * @return builder
         */
        public Builder mobile(String mobile) {
            this.mobile = mobile;
            return this;
        }

        /**
         * 设置模板参数
         *
         * @param para 模板参数
         * @return builder
         */
        public Builder para(JSONObject para) {
            this.para = para;
            return this;
        }

        /**
         * 设置接受对象，可不传
         *
         * @param receiver 接受对象
         * @return builder
         */
        public Builder receiver(@Nullable String receiver) {
            this.receiver = receiver;
            return this;
        }

        /**
         * 设置是否需要记录
         *
         * @param needRecord 需要记
         * @return builder
         */
        public Builder needRecord(boolean needRecord) {
            this.needRecord = needRecord;
            return this;
        }

        /**
         * 构建
         *
         * @return 单个短信体
         */
        public SingleSmsBody build() {
            if (templateCode == null) {
                throw new IllegalArgumentException("templateCode should not be null");
            }
            if (mobile == null) {
                throw new IllegalArgumentException("mobile should not be null");
            }
            if (para == null) {
                throw new IllegalArgumentException("para should not be null");
            }
            SingleSmsBody singleSmsBody = new SingleSmsBody();
            singleSmsBody.setTemplateCode(templateCode);
            singleSmsBody.para = this.para;
            singleSmsBody.receiver = this.receiver;
            singleSmsBody.needRecord = this.needRecord;
            singleSmsBody.mobile = this.mobile;
            return singleSmsBody;
        }
    }
}
