package com.fanruan.api.util.trans;

import com.fr.base.sms.SMSManager;
import com.fr.json.JSONArray;

import java.util.List;

/**
 * 批量短信体
 *
 * @author vito
 * @date 2019-07-24
 */
public class BatchSmsBody extends BaseSmsBody {
    private List<String> mobiles;
    private List<String> receivers;
    private JSONArray para;

    private BatchSmsBody() {
    }

    public List<String> getMobiles() {
        return mobiles;
    }

    public List<String> getReceivers() {
        return receivers;
    }

    public JSONArray getPara() {
        return para;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public boolean send() throws Exception {
        return SMSManager.getInstance().batchSendSMS(
                getTemplateCode(),
                getMobiles(),
                getPara(),
                getReceivers());
    }

    public static final class Builder {
        private String templateCode;
        private List<String> mobiles;
        private List<String> receivers;
        private JSONArray para;

        private Builder() {
        }

        public Builder templateCode(String templateCode) {
            this.templateCode = templateCode;
            return this;
        }

        public Builder mobiles(List<String> mobiles) {
            this.mobiles = mobiles;
            return this;
        }

        public Builder receivers(List<String> receivers) {
            this.receivers = receivers;
            return this;
        }

        public Builder para(JSONArray para) {
            this.para = para;
            return this;
        }

        public BatchSmsBody build() {
            if (templateCode == null) {
                throw new IllegalArgumentException("templateCode should not be null");
            }
            if (mobiles == null) {
                throw new IllegalArgumentException("mobiles should not be null");
            }
            if (para == null) {
                throw new IllegalArgumentException("para should not be null");
            }
            BatchSmsBody batchSmsBody = new BatchSmsBody();
            batchSmsBody.setTemplateCode(templateCode);
            batchSmsBody.receivers = this.receivers;
            batchSmsBody.mobiles = this.mobiles;
            batchSmsBody.para = this.para;
            return batchSmsBody;
        }
    }
}
