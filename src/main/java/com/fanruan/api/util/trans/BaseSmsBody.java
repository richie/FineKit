package com.fanruan.api.util.trans;

/**
 * 抽象短信体
 *
 * @author vito
 * @date 2019-07-24
 */
public abstract class BaseSmsBody {
    private String templateCode;

    public String getTemplateCode() {
        return templateCode;
    }

    void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    /**
     * 发送
     *
     * @return 发送结果
     * @throws Exception 发送异常
     */
    public abstract boolean send() throws Exception;
}
