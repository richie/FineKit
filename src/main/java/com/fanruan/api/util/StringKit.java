package com.fanruan.api.util;

import com.fr.stable.StringUtils;
import java.io.UnsupportedEncodingException;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 * 字符串工具类
 */
public class StringKit {

    public static final String EMPTY = "";
    public static final String BLANK = " ";

    /**
     * <p>检查一个字符串是否是空字符串</p>
     * <p/>
     * <pre>
     * StringKit.isEmpty(null)      = true
     * StringKit.isEmpty("")        = true
     * StringKit.isEmpty(" ")       = false
     * StringKit.isEmpty("bob")     = false
     * StringKit.isEmpty("  bob  ") = false
     * </pre>
     * <p/>
     *
     * @param text 被检查的字符串，可能为null
     * @return 如果字符串为空或者是null则返回true，否则返回false
     */
    public static boolean isEmpty(String text) {
        return StringUtils.isEmpty(text);
    }

    /**
     * <p>检查一个字符串是否不为空字符串</p>
     * <p/>
     * <pre>
     * StringKit.isNotEmpty(null)      = false
     * StringKit.isNotEmpty("")        = false
     * StringKit.isNotEmpty(" ")       = true
     * StringKit.isNotEmpty("bob")     = true
     * StringKit.isNotEmpty("  bob  ") = true
     * </pre>
     *
     * @param text 被检查的字符串，可能是null
     * @return 如果字符串不为空且不是null则返回true，否则返回false
     */
    public static boolean isNotEmpty(String text) {
        return StringUtils.isNotEmpty(text);
    }

    /**
     * <p>检查一个字符串是否为空白字符串</p>
     * <p/>
     * <pre>
     * StringKit.isBlank(null)      = true
     * StringKit.isBlank("")        = true
     * StringKit.isBlank(" ")       = true
     * StringKit.isBlank("bob")     = false
     * StringKit.isBlank("  bob  ") = false
     * </pre>
     *
     * @param text 被检查的字符串
     * @return 如果字符串为空、空格符或者null那么返回true，否则返回false
     */
    public static boolean isBlank(String text) {
        return StringUtils.isBlank(text);
    }

    /**
     * <p>检查一个字符串是否不是空白字符串</p>
     * <p/>
     * <pre>
     * StringKit.isNotBlank(null)      = false
     * StringKit.isNotBlank("")        = false
     * StringKit.isNotBlank(" ")       = false
     * StringKit.isNotBlank("bob")     = true
     * StringKit.isNotBlank("  bob  ") = true
     * </pre>
     *
     * @param text 被检查的字符串
     * @return 如果字符串不是空字符串、空格符以及null那么就返回true，否则返回false
     */
    public static boolean isNotBlank(String text) {
        return StringUtils.isNotBlank(text);
    }
    /**
     * 对字符串做trim操作
     *
     * @param string 源字符串
     * @return 返回操作后的字符串
     */
    public static String trim(String string) {
        return StringUtils.trim(string);
    }

    /**
     *如果String为空则返回空字符串，否则返回String本身的值，保证String不空。
     * @param text
     * @return
     */
    public static String alwaysNotNull(String text) {
        return StringUtils.alwaysNotNull(text);
    }
    /**
     * 获取text字符串中去除结尾suffix字符串的部分
     * @param text 源字符串
     * @param suffix 后缀字符串
     * @return 若text字符串结尾是suffix，则返回去除suffix的部分，否则返回text
     */
    public static String cutStringEndWith(String text, String suffix) {
        return StringUtils.cutStringEndWith(text, suffix);
    }

    /**获取text字符串去除开头startString部分
     *
     * @param text 源字符串
     * @param startString 前缀字符串
     * @return 若text字符串开头是suffix，则返回去除startString部分否则返回text
     */
    public static String cutStringStartWith(String text, String startString) {
        return StringUtils.cutStringStartWith(text, startString);
    }

    /**
     * 去掉text字符串两边的空格或ASCII码小于空格的字符
     * @param text 源字符串
     * @return 若text只含空格字符，或ASCII码小于空格的字符，返回null，否则返回去除两边空字符后的text
     */
    public static String trimToNull(String text) {
        return StringUtils.trimToNull(text);
    }

    /**
     * 令text以startString开头
     * @param text 源字符串
     * @param startString 前缀字符串
     * @return 若text不以startString开头，在text前添加startString。若text以startString开头返回text
     */
    public static String perfectStart(String text, String startString) {
        return StringUtils.perfectStart(text, startString);
    }

    /**
     * 令text以suffix结尾
     * @param text 源字符串
     * @param suffix 前缀字符串
     * @return 若text不以suffix结尾，在text后添加suffix。若text以suffix结尾返回text
     */
    public static String perfectEnd(String text, String suffix) {
        return StringUtils.perfectEnd(text, suffix);
    }

    /**
     * 令text的开头结尾都是str
     * @param text
     * @param str
     * @return 若text的开头或结尾不是str，在相应位置添加str，若开头结尾都是text，返回text
     */
    public static String perfectSurround(String text, String str) {
        return StringUtils.perfectSurround(text, str);
    }

    /**
     * 返回text长度
     * @param text 源字符串
     * @return 若text为null返回0，否则返回text长度
     */
    public static int getLength(String text) {
        return StringUtils.getLength(text);
    }

    /**
     * 判断text1和text2在去掉开头或结尾的ignoreStr部分是否相等
     * @param text1 源字符串
     * @param text2 待比较字符串
     * @param ignoreStr 可以忽略的字符串
     * @return 如果text1和txet2去掉开头结尾的ignoreStr后相等返回true，不等返回false
     */
    public static boolean equalsIgnore(String text1, String text2, String ignoreStr) {
        return StringUtils.equalsIgnore(text1, text2, ignoreStr);
    }

    /**
     * 将str加入到strArr字符串数组的每个字符串后方（不包括最后一个字符串），同时将结果字符串数组连接起来，形成新的字符串
     * @param str 源字符串
     * @param strArr 待连接字符串数组
     * @return 若strArr为空则返回null，若strArr长度为0返回""空字符串，否则返回连接后的字符串
     */
    public static String join(String str, String[] strArr) {
        return StringUtils.join(str, strArr);
    }

    /**
     * 将text字符串中的A-J转换成0-9,工具函数
     * @param text 源字符串
     * @return 返回转换后的字符串
     */
    public static String parseVersion(String text) {
        return StringUtils.parseVersion(text);
    }

    /**
     * 判断text是否是json中数组的string形式
     * @param text 源字符串
     * @return 若是数组的String形式返回true，否则返回flase
     */
    public static boolean isArrayType(String text) {
        return StringUtils.isArrayType(text);
    }

    /**
     *从原本json数组类型的String取出其中的每一个用逗号间隔元素，作为String二维数组返回值
     * @param text 源字符串
     * @return 取出text中用逗号间隔的元素
     */
    public static String[][] stringToArray(String text) {
        return StringUtils.stringToArray(text);
    }

    /**
     *对text中每个字符，用charsetName为名的编码方式解码后，返回解码后字节长度从0到bytelength的部分，以String形式返回
     * @param text 源字符串
     * @param charsetName 编码方式
     * @param byteLength 字节长度
     * @return 若text为空或byteLength小于等于0返回空字符串，否则以String形式返回解码后字节长度从0到bytelength的部分
     */
    public static String subStringByByteLength(String text, String charsetName, int byteLength) throws UnsupportedEncodingException {
        return StringUtils.subStringByByteLength(text, charsetName, byteLength);
    }

    /**
     * 比较text和compareStr两字符串值是否相等
     * @param text 源字符串
     * @param compareStr 待比较字符串
     * @return 若text和compareStr两字符串的值相同返回true，否则返回false
     */
    public static boolean equals(String text, String compareStr) {
        return StringUtils.equals(text, compareStr);
    }

    /**
     * 比较text和compareStr两字符串忽略大小写的情况下值是否相等
     * @param text 源字符串
     * @param compareStr 待比较字符串
     * @return 在忽略大小写的情况下，若text和compareStr两字符串的值相同返回true，否则返回false
     */
    public static boolean equalsIgnoreCase(String text, String compareStr) {
        return StringUtils.equalsIgnoreCase(text, compareStr);
    }

    /**
     * 扩展text的长度至stringLength，不足则以空格填充
     * @param text 源字符串
     * @param stringLength 字符串扩展长度
     * @return 若text长度比stringLength，则将text扩充到stringLength长度，否则返回text
     */
    public static String rightPad(String text, int stringLength) {
        return StringUtils.rightPad(text, stringLength);
    }

}
