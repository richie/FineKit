package com.fanruan.api.util;

import com.fr.stable.ArrayUtils;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-16
 */
public class ArrayKit {

    public static final Object[] EMPTY_OBJECT_ARRAY = ArrayUtils.EMPTY_OBJECT_ARRAY;

    public static final Class<?>[] EMPTY_CLASS_ARRAY = ArrayUtils.EMPTY_CLASS_ARRAY;

    public static final String[] EMPTY_STRING_ARRAY = ArrayUtils.EMPTY_STRING_ARRAY;

    public static final long[] EMPTY_LONG_ARRAY = ArrayUtils.EMPTY_LONG_ARRAY;

    public static final Long[] EMPTY_LONG_OBJECT_ARRAY = ArrayUtils.EMPTY_LONG_OBJECT_ARRAY;

    public static final int[] EMPTY_INT_ARRAY = ArrayUtils.EMPTY_INT_ARRAY;

    public static final Integer[] EMPTY_INTEGER_OBJECT_ARRAY = ArrayUtils.EMPTY_INTEGER_OBJECT_ARRAY;

    public static final short[] EMPTY_SHORT_ARRAY = ArrayUtils.EMPTY_SHORT_ARRAY;

    public static final Short[] EMPTY_SHORT_OBJECT_ARRAY = ArrayUtils.EMPTY_SHORT_OBJECT_ARRAY;

    public static final byte[] EMPTY_BYTE_ARRAY = ArrayUtils.EMPTY_BYTE_ARRAY;

    public static final Byte[] EMPTY_BYTE_OBJECT_ARRAY = ArrayUtils.EMPTY_BYTE_OBJECT_ARRAY;

    public static final double[] EMPTY_DOUBLE_ARRAY = ArrayUtils.EMPTY_DOUBLE_ARRAY;

    public static final Double[] EMPTY_DOUBLE_OBJECT_ARRAY = ArrayUtils.EMPTY_DOUBLE_OBJECT_ARRAY;

    public static final float[] EMPTY_FLOAT_ARRAY = ArrayUtils.EMPTY_FLOAT_ARRAY;

    public static final Float[] EMPTY_FLOAT_OBJECT_ARRAY = ArrayUtils.EMPTY_FLOAT_OBJECT_ARRAY;

    public static final boolean[] EMPTY_BOOLEAN_ARRAY = ArrayUtils.EMPTY_BOOLEAN_ARRAY;

    public static final Boolean[] EMPTY_BOOLEAN_OBJECT_ARRAY = ArrayUtils.EMPTY_BOOLEAN_OBJECT_ARRAY;

    public static final char[] EMPTY_CHAR_ARRAY = ArrayUtils.EMPTY_CHAR_ARRAY;

    public static final Character[] EMPTY_CHARACTER_OBJECT_ARRAY = ArrayUtils.EMPTY_CHARACTER_OBJECT_ARRAY;

    /**
     * 判断数组是否为空
     *
     * @param array 源数组
     * @return 返回数组判断为空的结果
     */
    public static boolean isEmpty(Object[] array) {
        return ArrayUtils.isEmpty(array);
    }
    /**
     * 移除目标数组中对应下标的元素
     *
     * @param array 源数组
     * @param number 要移除的元素的下标
     * @return 返回移除元素后的数组
     */
    public static <T> T[] remove(T[] array, int number) {
        return ArrayUtils.remove(array, number);
    }
    /**
     * 给目标数组中增加单个元素
     *
     * @param array 源数组
     * @param element 要增加的元素
     * @return 返回增加元素后的数组
     */
    public static <T> T[] add(T[] array, T element) {return ArrayUtils.add(array, element);};
    /**
     * 给目标数组中增加多个元素
     *
     * @param array 源数组
     * @param element 要增加的元素
     * @return 返回增加元素后的数组
     */
    public static <T> T[] addAll(T[] array, T... element){return ArrayUtils.addAll(array, element);};

    /**
     * 判断数组是否为不空
     * @param array 原数组
     * @return 若数组array不空返回true，否则返回false
     */
    public static <T> boolean isNotEmpty(T[] array) {
        return ArrayUtils.isNotEmpty(array);
    }
}
