package com.fanruan.api.runtime;

import com.fr.plugin.context.PluginContexts;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-30
 * 插件相关的工具类
 */
public class PluginKit {

    /**
     * 当前插件是否可用（例如：插件授权文件过期时，该犯法会返回false）
     * @return 插件可用则返回true，不可用则返回false
     */
    public static boolean isCurrentPluginAvailable() {
        return PluginContexts.currentContext().isAvailable();
    }
}
