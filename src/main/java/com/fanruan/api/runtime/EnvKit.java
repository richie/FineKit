package com.fanruan.api.runtime;

import com.fr.stable.project.ProjectConstants;
import com.fr.workspace.WorkContext;
import com.fr.workspace.Workspace;
import com.fr.workspace.resource.WorkResource;
import com.fr.workspace.simple.SimpleWork;
import org.jetbrains.annotations.NotNull;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 */
public class EnvKit {

    /**
     * 获取当前的工作目录
     * @return 工作目录
     */
    public static Workspace getCurrent() {
        return WorkContext.getCurrent();
    }

    /**
     * 获取当前工作目录的资源读写对象
     * @return 资源读写对象
     */
    public static WorkResource getWorkResource() {
        return WorkContext.getWorkResource();
    }

    /**
     * 设置一个目录为工作目录
     * @param path 工作目录位置
     */
    public static void setCurrent(@NotNull String path) {
        if (path.endsWith(ProjectConstants.WEBINF_NAME)) {
            SimpleWork.checkIn(path);
        } else {
            throw new IllegalArgumentException("Cannot set " + path + " as work directory.");
        }
    }

    /**
     * 退出当前工作目录
     */
    public static void checkout() {
        SimpleWork.checkOut();
    }
}
