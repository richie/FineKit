package com.fanruan.api.xml.component;

import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;

public abstract class XMLFileManager extends com.fr.file.XMLFileManager {

    @Override
    public void writeXML(XMLPrintWriter writer) {

    }
    @Override
    public void readXML(XMLableReader reader) {

    }
}
