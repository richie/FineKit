package com.fanruan.api.xml;

import com.fr.data.core.DataCoreXmlUtils;
import com.fr.data.impl.Connection;
import com.fr.general.xml.GeneralXMLTools;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLReadable;
import com.fr.stable.xml.XMLTools;
import com.fr.stable.xml.XMLable;
import com.fr.stable.xml.XMLableReader;

import java.io.InputStream;

/**
 * xml读写相关类
 */
public class XmlKit {
    /**
     * 从输入流中读取对象
     *
     * @param read xml读取对象
     * @param in   xml输入流
     */
    public static void readInputStreamXML(XMLReadable read, InputStream in) throws Exception {
        XMLTools.readInputStreamXML(read, in);
    }


    /**
     * 将xml读取为具体的java对象
     *
     * @param reader xml读取器
     * @return java对象
     */
    public static XMLable readXMLable(XMLableReader reader) {
        return GeneralXMLTools.readXMLable(reader);
    }

    /**
     * 将java对象写为xml文件
     *
     * @param writer  xml写入器
     * @param xml     实际java对象
     * @param tagName xml标签名
     */
    public static void writeXMLable(XMLPrintWriter writer, XMLable xml, String tagName) {
        GeneralXMLTools.writeXMLable(writer, xml, tagName);
    }

    /**
     * 读取connection对象
     *
     * @param reader XMLableReader对象
     * @return Connection对象
     */
    public static Connection readXMLConnectionMap(XMLableReader reader) {
        return DataCoreXmlUtils.readXMLConnectionMap(reader);
    }

    /**
     * 从xml中读取数据连接对象
     * @param reader xml读取器
     * @return 数据连接对象
     */
    public static Connection readXMLConnection(XMLableReader reader) {
        return DataCoreXmlUtils.readXMLConnection(reader);
    }

    /**
     * 写connection对象
     *
     * @param writer     XMLPrintWriter对象
     * @param connection Connection对象
     */
    public static void writeXMLConnection(XMLPrintWriter writer, Connection connection) {
        DataCoreXmlUtils.writeXMLConnection(writer, connection);
    }

}
