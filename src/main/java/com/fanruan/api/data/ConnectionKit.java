package com.fanruan.api.data;

import com.fanruan.api.util.TypeKit;
import com.fr.data.impl.Connection;
import com.fr.data.impl.NameDatabaseConnection;
import com.fr.file.ConnectionConfig;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 数据连接相关工具类
 */
public class ConnectionKit {

    /**
     * 获取指定名字的数据连接
     *
     * @param name 名字
     * @return 数据连接
     */
    public static @Nullable Connection getConnection(@NotNull String name) {
        return ConnectionConfig.getInstance().getConnection(name);
    }

    /**
     * 获取指定名字和指定类型的数据连接
     *
     * @param name 数据连接的名字
     * @param type 类型
     * @return 数据连接
     */
    public static <T extends Connection> @Nullable T getConnection(@NotNull String name, Class<? extends Connection> type) {
        Connection connection = getConnection(name);
        if (TypeKit.objectInstanceOf(connection, type)) {
            return (T) connection;
        }
        return null;
    }

    /**
     * 生成一个使用名字来引用的数据连接对象
     *
     * @param name 数据连接的名字
     * @return 数据连接
     */
    public static Connection createNameConnection(String name) {
        return new NameDatabaseConnection(name);
    }
}
