package com.fanruan.api.data;

import com.fr.base.TableData;
import com.fr.data.TableDataSource;
import com.fr.data.api.TableDataAssist;
import com.fr.file.TableDataConfig;
import com.fr.script.Calculator;
import com.fr.stable.script.CalculatorProvider;
import com.fr.third.guava.collect.Maps;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-08-09
 * 数据集获取工具类
 */
public class TableDataKit {

    /**
     * 根据名字获取数据集
     * 根据以下步骤获取数据集：
     * 1、先查询当前模板，是否具有该名字的数据集，如果有，则返回，如果没有，则进行第二步；
     * 2、再查询服务器数据集配置，是否具有该名字的服务器数据集，如果有，则返回，如果没有，则返回null。
     *
     * @param cal  算子
     * @param name 数据集名字
     * @return 数据集
     */
    public static @Nullable TableData getTableData(@NotNull CalculatorProvider cal, @NotNull String name) {
        return TableDataAssist.getTableData((Calculator) cal, name);
    }

    /**
     * 根据名字获取数据集
     * 根据以下步骤获取数据集：
     * 1、先查询当前模板，是否具有该名字的数据集，如果有，则返回，如果没有，则进行第二步；
     * 2、再查询服务器数据集配置，是否具有该名字的服务器数据集，如果有，则返回，如果没有，则返回null。
     *
     * @param source 模板
     * @param name 数据集名字
     * @return 数据集
     */
    public static @Nullable TableData getTableData(@NotNull TableDataSource source, @NotNull String name) {
        return TableDataAssist.getTableData(source, name);
    }

    /**
     * 获取所有的服务器数据集
     * @return 所有的服务器数据集组成的键值对集合
     */
    public static @NotNull Map<String, TableData> getAllServerTableData() {
        Map<String, TableData> maps = TableDataConfig.getInstance().getTableDatas();
        return Maps.newHashMap(maps);
    }

    /**
     * 根据名字获取服务器数据集
     * @param name 待查找的服务器数据集的名字
     * @return 服务器数据集，如果没有这个名字的服务器数据集，则返回null
     */
    public static @Nullable TableData findTableData(@NotNull String name) {
        return TableDataConfig.getInstance().getTableData(name);
    }

    /**
     * 重命名数据集
     * @param oldName 原来的名字
     * @param newName 新名字
     */
    public static void renameTableData(@NotNull String oldName, @NotNull String newName) {
        TableDataConfig.getInstance().renameTableData(oldName, newName);
    }
}
