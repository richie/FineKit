# 帆软插件开发工具套件

## 作用简介
该套件在帆软工程类基础上做了二次封装，仅暴露接口，可以更好的兼容不同版本的非开放API。

比如，要获取报表的会话信息，以前需要调用方法
```java
import com.fr.web.core.SessionPoolManager;
import org.jetbrains.annotations.NotNull;

SessionPoolManager#getSessionIDInfor(@NotNull String sessionID, Class<T> clazz);
```
但可能随着版本的变更，SessionPoolManager变更了一个名字，比如由SessionPoolManager改成了NewSessionPoolManager，
那么所有调用了该API的插件都无法使用，必须要重新修改插件中的该处代码并更新插件才行。

而如果采用可开发工具套件中的方法，则始终都是调用
```java
import com.fanruan.api.session.SessionKit;
import org.jetbrains.annotations.NotNull;

SessionKit#getSession(@NotNull String sessionID);
```
仅仅只需要更新最新版本的开发套件工具包即可让老插件自然适配。

## 提交新API要求

* 所有的API方法的返回值和参数，仅允许使用JDK自带的类、接口类型、插件抽象类、注解或者使用了@Open标记的对象类型。

* 所有的API方法均需要有单元测试覆盖。

* 所有的API方法均需要有javadoc文档说明。

## 使用方法

先使用maven打包，执行下面的命令（跳过单元测试）

```
 mvn package -Dmaven.test.skip=true
```

会在target目录下获得一个形如finekit-10.0-20190815.jar名字jar包，直接作为插件依赖jar包即可。

## 已完全依赖FineKit的插件

|插件名|源码|
|-----|----|
|redis数据集插件|https://git.fanruan.com/fanruan/demo-tabledata-redis|
|增强公式编辑器插件|https://git.fanruan.com/fanruan/demo-formula-script
|条件属性之文本对齐插件|https://git.fanruan.com/fanruan/demo-highlight-align|

## 如何判断插件中调用的API需要增加到FineKit中

最简单的原则：除了JDK自带的类、插件接口（包括抽象类）、以及使用了@Open注解标记的类之外，是否还使用了com.fanruan.api（com.fr.third除外）之外的类，如果有则需要修改。

## 如何提交新的API

1、先访问[https://git.fanruan.com](https://git.fanruan.com)并注册一个自己的账号；

2、然后将[https://git.fanruan.com/fanruan/finekit](https://git.fanruan.com/fanruan/finekit) fork到自己账户下；

3、克隆FineKit代码到本地，新增相关的API代码，并提交到自己的仓库；

4、提交Pull Request到FineKit主仓库，等待审核通过被合并即可。